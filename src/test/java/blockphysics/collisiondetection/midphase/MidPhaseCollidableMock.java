/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.midphase;

import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public class MidPhaseCollidableMock implements MidPhaseCollidable{

    private final Vector3dJME centre;
    private final Vector3dJME halfExtents;

    public MidPhaseCollidableMock(Vector3dJME centre, Vector3dJME halfExtents) {
        this.centre = centre;
        this.halfExtents = halfExtents;
    }
    
    
    
    @Override
    public Vector3dJME getCollisionalCentrePosition() {
        return centre;
    }

    @Override
    public Vector3dJME getMaxRadiusForAxes() {
        return halfExtents;
    }
    
}
