/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.midphase;

import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class MidPhaseTest {
    
    public MidPhaseTest() {
    }

    /**
     * Test of midPhaseFilterCollisions method, of class MidPhase.
     */
    @Test
    public void testMidPhaseFilterCollisions() {
        MidPhase<MidPhaseCollidable> midPhase=new MidPhase<>(Executors.newFixedThreadPool(2),2);
        
        PotentialCollision<MidPhaseCollidable> collidingOne=
                new PotentialCollision<>(
                        new MidPhaseCollidableMock(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)),
                        new MidPhaseCollidableMock(new Vector3dJME(0.5,0.5,0.5),new Vector3dJME(1,1,1))
                );
        
        PotentialCollision<MidPhaseCollidable> collidingTwo=
                new PotentialCollision<>(
                        new MidPhaseCollidableMock(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)),
                        new MidPhaseCollidableMock(new Vector3dJME(1.9,0,0),new Vector3dJME(1,1,1))
                );
        
        PotentialCollision<MidPhaseCollidable> collidingThree=
                new PotentialCollision<>(
                        new MidPhaseCollidableMock(new Vector3dJME(0,0,0),new Vector3dJME(1,0.01,0.01)),
                        new MidPhaseCollidableMock(new Vector3dJME(1.9,0,0),new Vector3dJME(1,0.01,0.01))
                );
        
        PotentialCollision<MidPhaseCollidable> notCollidingOne=
                new PotentialCollision<>(
                        new MidPhaseCollidableMock(new Vector3dJME(0,0,0),new Vector3dJME(0.01,0.5,0.01)),
                        new MidPhaseCollidableMock(new Vector3dJME(1.9,0,0),new Vector3dJME(0.01,0.6,0.01))
                );
        
        PotentialCollision<MidPhaseCollidable> notCollidingTwo=
                new PotentialCollision<>(
                        new MidPhaseCollidableMock(new Vector3dJME(0,0,0),new Vector3dJME(0.4,0.4,0.4)),
                        new MidPhaseCollidableMock(new Vector3dJME(1,1,1),new Vector3dJME(0.4,0.4,0.4))
                );
        
        Collection<PotentialCollision<MidPhaseCollidable>> input=new ArrayList<>();
        
        input.add(collidingOne);
        input.add(collidingTwo);
        input.add(collidingThree);
        input.add(notCollidingOne);
        input.add(notCollidingTwo);

        Collection<PotentialCollision<MidPhaseCollidable>> potentiallyColliding=midPhase.midPhaseFilterCollisions(input);
        
        assertTrue(potentiallyColliding.contains(collidingOne));
        assertTrue(potentiallyColliding.contains(collidingTwo));
        assertTrue(potentiallyColliding.contains(collidingThree));
        assertFalse(potentiallyColliding.contains(notCollidingOne));
        assertFalse(potentiallyColliding.contains(notCollidingTwo));
        
        //just to gaurd against duplication etc
        
        assertEquals(3,potentiallyColliding.size());
    }
    
}
