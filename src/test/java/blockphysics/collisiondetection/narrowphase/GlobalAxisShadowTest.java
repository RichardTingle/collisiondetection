/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richard
 */
public class GlobalAxisShadowTest {
    
    public GlobalAxisShadowTest() {
    }


    @Test
    public void testOverlapsWith() {
        GlobalAxisShadow shadow1 =new GlobalAxisShadow(2,5);
        GlobalAxisShadow shadow2 =new GlobalAxisShadow(3,7);
        GlobalAxisShadow shadow3 =new GlobalAxisShadow(6,12);
        
        assertTrue(shadow1.overlapsWith(shadow2));
        assertTrue(shadow2.overlapsWith(shadow1));
        assertTrue(shadow2.overlapsWith(shadow3));
        assertTrue(shadow3.overlapsWith(shadow2));
        
        assertFalse(shadow1.overlapsWith(shadow3));
        assertFalse(shadow3.overlapsWith(shadow1));
        
    }
    
    @Test
    public void testgetOverlapShadow(){
        GlobalAxisShadow shadow1 =new GlobalAxisShadow(2,5);
        GlobalAxisShadow shadow2 =new GlobalAxisShadow(3,7);
        GlobalAxisShadow shadow3 =new GlobalAxisShadow(6,12);
        GlobalAxisShadow shadow4 =new GlobalAxisShadow(2,5);
        GlobalAxisShadow shadow5 =new GlobalAxisShadow(12,14);
        
        assertTrue(new AxisShadowOverlap(5,2,false).equals(shadow1.getOverlapShadow(shadow4),0.001));
        assertTrue(new AxisShadowOverlap(3,5,false).equals(shadow1.getOverlapShadow(shadow2),0.001));
        assertEquals(null,shadow1.getOverlapShadow(shadow3));
        assertTrue(new AxisShadowOverlap(6,7,false).equals(shadow2.getOverlapShadow(shadow3),0.001));
        assertTrue(new AxisShadowOverlap(12,12,false).equals(shadow3.getOverlapShadow(shadow5),0.001));
        
        GlobalAxisShadow shadow6 =new GlobalAxisShadow(2,5);
        GlobalAxisShadow shadow7 =new GlobalAxisShadow(0,100);
        
        assertTrue(new AxisShadowOverlap(5,2,true).equals(shadow6.getOverlapShadow(shadow7),0.001));
        
    }

}
