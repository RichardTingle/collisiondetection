/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.collisiondetection.ActualCollision;
import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.collisiondetection.collisionshape.Box;
import blockphysics.maths.QuaternionD;
import blockphysics.maths.Vector3dJME;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richard
 */
public class NarrowPhaseTest {
    
    public NarrowPhaseTest() {
    }

    /**
     * Test of getCollisionData method, of class NarrowPhase_BoxLevel.
     */
    @Test
    public void testCollidesUnrotated() {

        
        NarrowPhase<UnrotatedNarrowPhaseCollidableMock> narrowPhase = new NarrowPhase<>(Executors.newFixedThreadPool(2),2);
        
        Collection<Box> boxes = new HashSet<>();
        boxes.add(new Box(new Vector3dJME(0, 0, 0),new Vector3dJME(10,10,10)));
        boxes.add(new Box(new Vector3dJME(10,10,10),new Vector3dJME(20,20,20)));
        boxes.add(new Box(new Vector3dJME(20,20,20),new Vector3dJME(30,30,30)));
        boxes.add(new Box(new Vector3dJME(30,30,30),new Vector3dJME(40,40,40)));
        UnrotatedNarrowPhaseCollidableMock collider1=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,0,0));
        UnrotatedNarrowPhaseCollidableMock collider2=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(35,35,35));
        UnrotatedNarrowPhaseCollidableMock collider3=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,30,0));
        
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> collides1=new PotentialCollision<>(collider1,collider2);
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> doesntCollide1=new PotentialCollision<>(collider1,collider3);
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> doesntCollide2=new PotentialCollision<>(collider2,collider3);
        
        Set<PotentialCollision<UnrotatedNarrowPhaseCollidableMock>> potentialCollision = new HashSet<>();
        potentialCollision.add(collides1);
        potentialCollision.add(doesntCollide1);
        potentialCollision.add(doesntCollide2);
        
        Set<ActualCollision<UnrotatedNarrowPhaseCollidableMock>> colliding = narrowPhase.runNarrowPhase_multithreaded(potentialCollision);
       
        assertEquals(1,colliding.size());
    }

    
    @Test
    public void testCollidesUnrotated_2() {
        NarrowPhase<UnrotatedNarrowPhaseCollidableMock> narrowPhase = new NarrowPhase<>(Executors.newFixedThreadPool(2),2);
        

        Collection<Box> boxes = new HashSet<>();
        boxes.add(new Box(new Vector3dJME(0, 0, 0),new Vector3dJME(10,10,10)));
        UnrotatedNarrowPhaseCollidableMock collider1=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,0,0));
        UnrotatedNarrowPhaseCollidableMock collider2=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,9,0));
        UnrotatedNarrowPhaseCollidableMock collider3=new UnrotatedNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,-11,0));
        
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> collides1=new PotentialCollision<>(collider1,collider2);
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> doesntCollide1=new PotentialCollision<>(collider1,collider3);
        PotentialCollision<UnrotatedNarrowPhaseCollidableMock> doesntCollide2=new PotentialCollision<>(collider2,collider3);
        
        Set<PotentialCollision<UnrotatedNarrowPhaseCollidableMock>> potentialCollision = new HashSet<>();
        potentialCollision.add(collides1);
        potentialCollision.add(doesntCollide1);
        potentialCollision.add(doesntCollide2);
        
        Set<ActualCollision<UnrotatedNarrowPhaseCollidableMock>> colliding = narrowPhase.runNarrowPhase_multithreaded(potentialCollision);
       
        assertEquals(1,colliding.size());
        assertEquals(collides1,colliding.iterator().next().getPotentialCollision());
        
    }
    
    
    
    /**
     * Test of getCollisionData method, of class NarrowPhase_BoxLevel.
     */
    @Test
    public void testCollidesRotated() {
        QuaternionD fourtyFiveClockwise=QuaternionD.UNROTATED();
        fourtyFiveClockwise.fromAngleAxis(Math.PI/4.0, new Vector3dJME(0,1,0));
        
        QuaternionD unrotated=new QuaternionD();
        
        NarrowPhase<NarrowPhaseCollidable> narrowPhase = new NarrowPhase<>(Executors.newFixedThreadPool(2),2);
        
        Collection<PotentialCollision<NarrowPhaseCollidable>> input = new HashSet<>();
        
        Collection<Box> boxes = new HashSet<>();
        boxes.add(new Box(new Vector3dJME(-5, -5, -5),new Vector3dJME(5,5,5)));
        
        RotatableNarrowPhaseCollidableMock collider1=new RotatableNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,0,0),unrotated);
        RotatableNarrowPhaseCollidableMock collider2=new RotatableNarrowPhaseCollidableMock(boxes,new Vector3dJME(0,0,11),fourtyFiveClockwise); //wouldn't normally collide if it wasn't rotated 
        RotatableNarrowPhaseCollidableMock collider3=new RotatableNarrowPhaseCollidableMock(boxes,new Vector3dJME(-9.5,0,-9.5),fourtyFiveClockwise); //would normally collide is if wasn't rotated

        
        PotentialCollision<NarrowPhaseCollidable> collides1=new PotentialCollision<>(collider1,collider2);
        PotentialCollision<NarrowPhaseCollidable> doesntCollide1=new PotentialCollision<>(collider1,collider3);
        PotentialCollision<NarrowPhaseCollidable> doesntCollide2=new PotentialCollision<>(collider2,collider3);
        
        Set<PotentialCollision<NarrowPhaseCollidable>> potentialCollision = new HashSet<>();
        potentialCollision.add(collides1);
        potentialCollision.add(doesntCollide1);
        potentialCollision.add(doesntCollide2);
        
        Set<ActualCollision<NarrowPhaseCollidable>> colliding = narrowPhase.runNarrowPhase_multithreaded(potentialCollision);
       
        assertEquals(1,colliding.size());
    }
    
    
}
