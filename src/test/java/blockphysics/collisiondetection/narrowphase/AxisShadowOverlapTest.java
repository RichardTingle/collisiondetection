/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richard
 */
public class AxisShadowOverlapTest {
    
    public AxisShadowOverlapTest() {
    }

    /**
     * Test of hashCode method, of class AxisShadowOverlap.
     */
    @Test
    public void testGetOverlapShadow() {
        
        AxisShadowOverlap overlap=new GlobalAxisShadow(1,10).getOverlapShadow(new GlobalAxisShadow(9,100));
        
        assertEquals(1,overlap.getSignedShadowLength(),0.001);
                
        AxisShadowOverlap overlap2=new GlobalAxisShadow(9,100).getOverlapShadow(new GlobalAxisShadow(1,10));
        
        assertEquals(-1,overlap2.getSignedShadowLength(),0.001); 
    }


    
}
