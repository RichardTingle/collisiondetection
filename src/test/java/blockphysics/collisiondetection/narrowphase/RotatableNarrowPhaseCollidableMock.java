/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.collisiondetection.collisionshape.Box;
import blockphysics.collisiondetection.collisionshape.BreakDownBox;
import blockphysics.maths.QuaternionD;
import blockphysics.maths.Vector3dJME;
import java.util.Collection;

/**
 *
 * @author Richard
 */
public class RotatableNarrowPhaseCollidableMock implements NarrowPhaseCollidable{

    private final BreakDownBox bbBox;
    private final Vector3dJME zeroPosition;
    private final QuaternionD rotation;
    
    public RotatableNarrowPhaseCollidableMock(Collection<Box> boxes, Vector3dJME zeroPosition, QuaternionD rotation){
        this.bbBox=new BreakDownBox(boxes);
        this.zeroPosition=zeroPosition;
        this.rotation=rotation;
    }
    
    @Override
    public BreakDownBox getTopLevelBreakDownBox() {
        return bbBox;
    }

    @Override
    public Vector3dJME convertGlobalAxisToLocal(Vector3dJME axis) {
        return axis.rotateAsVector(rotation);
    }

    @Override
    public Vector3dJME getCOMPosition() {
        return zeroPosition;
    }

    @Override
    public double getAxisOffset(Vector3dJME globalAxis) {
        return globalAxis.dot(zeroPosition);
    }

    @Override
    public Vector3dJME[] getFaceAxes() {
        Vector3dJME[] faceAxes=new Vector3dJME[3];
        faceAxes[0]=new Vector3dJME(1,0,0);
        faceAxes[1]=new Vector3dJME(0,1,0);
        faceAxes[2]=new Vector3dJME(0,0,1);
        
        Vector3dJME[] faceAxes_rotated=new Vector3dJME[3];
        faceAxes_rotated[0]=faceAxes[0].rotateAsVector(rotation);
        faceAxes_rotated[1]=faceAxes[1].rotateAsVector(rotation);
        faceAxes_rotated[2]=faceAxes[2].rotateAsVector(rotation);
        
        
        return faceAxes_rotated;
    }
    
}
