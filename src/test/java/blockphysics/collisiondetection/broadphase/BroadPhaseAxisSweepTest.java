/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;


import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class BroadPhaseAxisSweepTest {
    
    public BroadPhaseAxisSweepTest() {
    }

    /**
     * Test of call method, of class BroadPhaseAxisSweep.
     * @throws java.lang.Exception
     */
    @Test
    public void testCall_xAxis() throws Exception {
        //we're testing if the axis sweep correctlt determines which collidables
        //overlap on an axis
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(-1,0,0),0.6,false);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(0,5,5),0.6,false);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(1,5,5),0.6,false); 
        
        List<BroadPhaseCollidable> presortedCollidables=new ArrayList<>();
        presortedCollidables.add(alpha);
        presortedCollidables.add(beta);
        presortedCollidables.add(gamma);
        
        
        
        BroadPhaseAxisSweep sweep=new BroadPhaseAxisSweep(SweepAxis.XAXIS,presortedCollidables);

        Set<PotentialCollision<BroadPhaseCollidable>> potentialCollisions = sweep.call();
        
        assertTrue("alpha and beta may collide",potentialCollisions.contains(new PotentialCollision<>(alpha,beta)) );
        assertTrue("beta and gamma may collide",potentialCollisions.contains(new PotentialCollision<>(beta,gamma)) );
        assertFalse("alpha and gamma will not collide",potentialCollisions.contains(new PotentialCollision<>(alpha,gamma)) );
    }
    
    /**
     * Test of call method, of class BroadPhaseAxisSweep.
     * @throws java.lang.Exception
     */
    @Test
    public void testCall_yAxis() throws Exception {
        //we're testing if the axis sweep correctlt determines which collidables
        //overlap on an axis
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(0,-1,0),0.6,false);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(5,0,5),0.6,false);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(5,1,5),0.6,false); 
        
        List<BroadPhaseCollidable> presortedCollidables=new ArrayList<>();
        presortedCollidables.add(alpha);
        presortedCollidables.add(beta);
        presortedCollidables.add(gamma);
        
        
        
        BroadPhaseAxisSweep sweep=new BroadPhaseAxisSweep(SweepAxis.YAXIS,presortedCollidables);

        Set<PotentialCollision<BroadPhaseCollidable>> potentialCollisions = sweep.call();
        
        assertTrue("alpha and beta may collide",potentialCollisions.contains(new PotentialCollision<>(alpha,beta)) );
        assertTrue("beta and gamma may collide",potentialCollisions.contains(new PotentialCollision<>(beta,gamma)) );
        assertFalse("alpha and gamma will not collide",potentialCollisions.contains(new PotentialCollision<>(alpha,gamma)) );
    }
    
    /**
     * Test of call method, of class BroadPhaseAxisSweep.
     * @throws java.lang.Exception
     */
    @Test
    public void testCall_zAxis() throws Exception {
        //we're testing if the axis sweep correctlt determines which collidables
        //overlap on an axis
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,-1),0.6,false);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(5,5,0),0.6,false);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(5,5,1),0.6,false); 
        
        List<BroadPhaseCollidable> presortedCollidables=new ArrayList<>();
        presortedCollidables.add(alpha);
        presortedCollidables.add(beta);
        presortedCollidables.add(gamma);
        
        
        
        BroadPhaseAxisSweep sweep=new BroadPhaseAxisSweep(SweepAxis.ZAXIS,presortedCollidables);

        Set<PotentialCollision<BroadPhaseCollidable>> potentialCollisions = sweep.call();
        
        assertTrue("alpha and beta may collide",potentialCollisions.contains(new PotentialCollision<>(alpha,beta)) );
        assertTrue("beta and gamma may collide",potentialCollisions.contains(new PotentialCollision<>(beta,gamma)) );
        assertFalse("alpha and gamma will not collide",potentialCollisions.contains(new PotentialCollision<>(alpha,gamma)) );
    }
    
    /**
     * Test of call method, of class BroadPhaseAxisSweep.
     * @throws java.lang.Exception
     */
    @Test
    public void testCall_staticsOnlyCollideWithNonStatics() throws Exception {
        //we're testing if the axis sweep correctlt determines which collidables
        //overlap on an axis
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,true);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,true);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,false); 
        
        List<BroadPhaseCollidable> presortedCollidables=new ArrayList<>();
        presortedCollidables.add(alpha);
        presortedCollidables.add(beta);
        presortedCollidables.add(gamma);
        

        BroadPhaseAxisSweep sweep=new BroadPhaseAxisSweep(SweepAxis.XAXIS,presortedCollidables);

        Set<PotentialCollision<BroadPhaseCollidable>> potentialCollisions = sweep.call();
        
        assertFalse("2 statics by defintion do not collide",potentialCollisions.contains(new PotentialCollision<>(alpha,beta)) );
        assertTrue("Statics do collide with non statics",potentialCollisions.contains(new PotentialCollision<>(alpha,gamma)) );
    }
    
    /**
     * Test of call method, of class BroadPhaseAxisSweep.
     * @throws java.lang.Exception
     */
    @Test
    public void testCall_deadCollidersArentIncluded() throws Exception {
        //we're testing if the axis sweep correctlt determines which collidables
        //overlap on an axis
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,true);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,true);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,false); 
        
        List<BroadPhaseCollidable> presortedCollidables=new ArrayList<>();
        presortedCollidables.add(alpha);
        presortedCollidables.add(beta);
        presortedCollidables.add(gamma);
        

        BroadPhaseAxisSweep sweep=new BroadPhaseAxisSweep(SweepAxis.XAXIS,presortedCollidables);

        Set<PotentialCollision<BroadPhaseCollidable>> potentialCollisions = sweep.call();
        
        assertFalse("2 statics by defintion do not collide",potentialCollisions.contains(new PotentialCollision<>(alpha,beta)) );
        assertTrue("Statics do collide with non statics",potentialCollisions.contains(new PotentialCollision<>(alpha,gamma)) );
    }
    
    
    
}
