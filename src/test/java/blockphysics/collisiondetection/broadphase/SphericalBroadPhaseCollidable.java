/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;

import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public class SphericalBroadPhaseCollidable implements BroadPhaseCollidable{

    private final Vector3dJME centre;
    private final double radius;
    private final boolean isStatic;
        
    public SphericalBroadPhaseCollidable(Vector3dJME centre, double radius, boolean isStatic) {
        this.centre = centre;
        this.radius = radius;
        this.isStatic=isStatic;
    }
    
    
    
    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public Vector3dJME getCollisionalCentrePosition() {
        return centre;
    }

    @Override
    public double getMaxRadius() {
        return radius;
    }

    @Override
    public boolean isStatic() {
        return isStatic;
    }

    @Override
    public boolean isCollidable() {
        return true;
    }

    @Override
    public String toString() {
        return "SphericalBroadPhaseCollidable{" + "centre=" + centre + ", radius=" + radius + ", isStatic=" + isStatic + '}';
    }
    
    
}
