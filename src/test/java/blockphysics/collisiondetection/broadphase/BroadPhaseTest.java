/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;

import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.maths.Vector3dJME;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class BroadPhaseTest {
    
    public BroadPhaseTest() {
    }


    /**
     * Test of runBroadPhase method, of class BroadPhase.
     */
    @Test
    public void testRunBroadPhase() {
        ExecutorService threadPoolDependancy= Executors.newFixedThreadPool(2);
        
        BroadPhase<BroadPhaseCollidable> broadPhase=new BroadPhase<>(threadPoolDependancy);
        
        BroadPhaseCollidable alpha=new SphericalBroadPhaseCollidable(new Vector3dJME(0,0,0),0.6,false);
        BroadPhaseCollidable beta=new SphericalBroadPhaseCollidable(new Vector3dJME(0.5,0,0),0.6,false);
        BroadPhaseCollidable gamma=new SphericalBroadPhaseCollidable(new Vector3dJME(0.5,0,5),0.6,false);

        
        broadPhase.registerObject(alpha);
        broadPhase.registerObject(beta);
        broadPhase.registerObject(gamma);
        
        Collection<PotentialCollision<BroadPhaseCollidable>> broadPhasePotentialCollisions = broadPhase.runBroadPhase();
        
        assertTrue("All 3 axis overlapping leads to a collision",broadPhasePotentialCollisions.contains(new PotentialCollision<>(alpha,beta)));
        assertFalse("Only 2 axis overlapping doesn't lead to a collision",broadPhasePotentialCollisions.contains(new PotentialCollision<>(alpha,gamma)));
    }
    
    
    /**
     * Test of runBroadPhase method, of class BroadPhase.
     */
   
    @Test
    public void testRunBroadPhase_deadObjectsNotIncluded() {
        ExecutorService threadPoolDependancy= Executors.newFixedThreadPool(2);
        
        BroadPhase broadPhase=new BroadPhase(threadPoolDependancy);
        
        BroadPhaseCollidable alpha=new DeadSphericalBroadPhaseCollidableMock(new Vector3dJME(0,0,0),0.6,false);
        BroadPhaseCollidable beta=new DeadSphericalBroadPhaseCollidableMock(new Vector3dJME(0,0,0),0.6,false);

        
        broadPhase.registerObject(alpha);
        broadPhase.registerObject(beta);

        
        Collection<PotentialCollision<BroadPhaseCollidable>> broadPhasePotentialCollisions = broadPhase.runBroadPhase();
        
        assertTrue("Dead objects aren't included in the collisions",broadPhasePotentialCollisions.isEmpty());

    }
    
}
