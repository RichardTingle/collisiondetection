/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;

import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public class DeadSphericalBroadPhaseCollidableMock extends SphericalBroadPhaseCollidable{
    
    public DeadSphericalBroadPhaseCollidableMock(Vector3dJME centre, double radius, boolean isStatic) {
        super(centre, radius, isStatic);
    }

    @Override
    public boolean isAlive() {
        return false;
    }
    
    
    
}
