/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.collisionshape;

import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class BoxBasedCollisionShapeTest {
    
    public BoxBasedCollisionShapeTest() {
    }


    /**
     * Test of getLocalGeometricalCentre method, of class BoxBasedCollisionShape.
     */
    @Test
    public void testGetLocalGeometricalCentre_localFrame() {
        
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)));
        boxes.add(new Box(new Vector3dJME(1,1,1),new Vector3dJME(4,5,6)));
        boxes.add(new Box(new Vector3dJME(1,1,1),new Vector3dJME(4,5,6)));
        
        BoxBasedCollisionShape collisionShape = new BoxBasedCollisionShape(boxes);

        Vector3dJME expected=new Vector3dJME(2,2.5,3);
        Vector3dJME actual=collisionShape.getLocalGeometricalCentre_localFrame();
        
        assertEquals(expected.x,actual.x,0.0001);
        assertEquals(expected.y,actual.y,0.0001); 
        assertEquals(expected.z,actual.z,0.0001); 
    }

    
    /**
     * Test of getmaxRadiusFromGeometricalCentre method, of class BoxBasedCollisionShape.
     */
    @Test
    public void testGetMaxRadiusFromGeometricalCentre() {
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)));
        boxes.add(new Box(new Vector3dJME(1,1,1),new Vector3dJME(6,4,2)));
        
        BoxBasedCollisionShape collisionShape = new BoxBasedCollisionShape(boxes);
        
        assertEquals(Math.sqrt(3*3+2*2+1*1),collisionShape.getmaxRadiusFromGeometricalCentre(),0.001);
        
    }

    /**
     * Test of getCornerPoints method, of class BoxBasedCollisionShape.
     */
    @Test
    public void testGetCornerPoints() {
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)));
        boxes.add(new Box(new Vector3dJME(1,1,1),new Vector3dJME(6,4,2)));
        
        BoxBasedCollisionShape collisionShape = new BoxBasedCollisionShape(boxes);
        
        Collection<Vector3dJME> corners=collisionShape.getCornerPoints();
        
        assertEquals(8,corners.size());
        
        assertTrue(corners.contains(new Vector3dJME(0,0,0)));
        assertTrue(corners.contains(new Vector3dJME(0,0,2)));
        assertTrue(corners.contains(new Vector3dJME(0,4,0)));
        assertTrue(corners.contains(new Vector3dJME(0,4,2)));
        assertTrue(corners.contains(new Vector3dJME(6,0,0)));
        assertTrue(corners.contains(new Vector3dJME(6,0,2)));
        assertTrue(corners.contains(new Vector3dJME(6,4,0)));
        assertTrue(corners.contains(new Vector3dJME(6,4,2)));
        
        
        
        
    }

    /**
     * Test of getAxisShadow method, of class BoxBasedCollisionShape.
     */
    @Test
    public void testGetAxisShadow_3args_1() {
    }

    /**
     * Test of getAxisShadow method, of class BoxBasedCollisionShape.
     */
    @Test
    public void testGetAxisShadow_3args_2() {
    }
    
}
