/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.collisionshape;

import blockphysics.collisiondetection.narrowphase.GlobalAxisShadow;
import blockphysics.collisiondetection.narrowphase.PhysicsReusuableObjects;
import blockphysics.collisiondetection.narrowphase.SeperatingAxisLocalFrame;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class BreakDownBoxTest {
    
    public BreakDownBoxTest() {
    }

    /**
     * Test of getBreakDownOverallMins_localFrame method, of class BreakDownBox.
     */
    @Test
    public void testGetBreakDownOverallMins_localFrame() {
        
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(10,   0,   10),new Vector3dJME(1000,1000,1000)));
        boxes.add(new Box(new Vector3dJME(10, -10,  100),new Vector3dJME(1000,1000,1000)));
        boxes.add(new Box(new Vector3dJME(0,    0,  100),new Vector3dJME(1000,1000,1000)));
        
        BreakDownBox bdBox=new BreakDownBox(boxes,4);
        
        assertEquals(new Vector3dJME(0,-10,10),bdBox.getBreakDownOverallMins_localFrame());
        
    }

    /**
     * Test of getBreakDownOverallMaxs_localFrame method, of class BreakDownBox.
     */
    @Test
    public void testGetBreakDownOverallMaxs_localFrame() {
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(-1000,-1000,-1000),new Vector3dJME(1000,-100,-100)));
        boxes.add(new Box(new Vector3dJME(-1000,-1000,-1000),new Vector3dJME(0,      0,-200)));
        boxes.add(new Box(new Vector3dJME(-1000,-1000,-1000),new Vector3dJME(0,   -100,-200)));
        
        BreakDownBox bdBox=new BreakDownBox(boxes,4);
        
        assertEquals(new Vector3dJME(1000,0,-100),bdBox.getBreakDownOverallMaxs_localFrame());
        
        
    }

    /**
     * Test of getBreakdownBoxShadow method, of class BreakDownBox.
     */
    @Test
    public void testGetBreakdownBoxShadow_nonLocallyAxisAligned() {
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0, 0, 0),new Vector3dJME(10,100,1000)));
        
        BreakDownBox bdBox=new BreakDownBox(boxes,4);
        
        GlobalAxisShadow shadowA=bdBox.getBreakdownBoxShadow(new SeperatingAxisLocalFrame(new Vector3dJME(0,1,1),2), new GlobalAxisShadow(), new PhysicsReusuableObjects());

        double distanceTo0_100_1000=Math.sqrt(1000*1000 + 100*100); //1005
        double angleTo0_100_1000=Math.atan2(100, 1000); //0.1 radians
        
        double angleToShadowEndPoint=Math.PI/4;
        
        double expectedLength=distanceTo0_100_1000*Math.cos(angleToShadowEndPoint-angleTo0_100_1000);
        
        assertEquals(0+2, shadowA.getStart(),0);
        assertEquals(expectedLength+2, shadowA.getEnd(),0.1);
         
        
    }

    /**
     * Test of getBreakdownBoxShadow method, of class BreakDownBox.
     * 
     * Tests the quick method for when the axis is X,Y or Z
     */
    @Test
    public void testGetBreakdownBoxShadow_locallyAxisAligned() {
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(-10, 0, 0),new Vector3dJME(10, 1, 1)));
        boxes.add(new Box(new Vector3dJME(-5, -6, 0),new Vector3dJME(5,  6, 1)));
        boxes.add(new Box(new Vector3dJME(-5,  0, -7),new Vector3dJME(5, 1, 7)));
        
        BreakDownBox bdBox=new BreakDownBox(boxes,4);
        
        GlobalAxisShadow axisShadowX=bdBox.getBreakdownBoxShadow(LocalCardinalAxis.X,0, new GlobalAxisShadow());
        GlobalAxisShadow axisShadowY=bdBox.getBreakdownBoxShadow(LocalCardinalAxis.Y,0, new GlobalAxisShadow());
        GlobalAxisShadow axisShadowZ=bdBox.getBreakdownBoxShadow(LocalCardinalAxis.Z,0, new GlobalAxisShadow());
        
        assertTrue(new GlobalAxisShadow(-10,10).equals(axisShadowX,0.001));
        assertTrue(new GlobalAxisShadow(-6,6).equals(axisShadowY,0.001));
        assertTrue(new GlobalAxisShadow(-7,7).equals(axisShadowZ,0.001));
    }

    /**
     * Test of isBottomLevelBox method, of class BreakDownBox.
     */
    @Test
    public void testIsBottomLevelBox() {
        
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1)));
        boxes.add(new Box(new Vector3dJME(10,0,0),new Vector3dJME(11,1,1)));
        boxes.add(new Box(new Vector3dJME(100,0,0),new Vector3dJME(101,1,1)));
        
        BreakDownBox bdBox_smallMinimum=new BreakDownBox(boxes,2);
        BreakDownBox bdBox_largeMinimum=new BreakDownBox(boxes,4);
        
        assertFalse("More than minimum size add, has substructure", bdBox_smallMinimum.isBottomLevelBox());
        assertTrue("More than minimum size add, has no substructure", bdBox_largeMinimum.isBottomLevelBox());
        
    }

    /**
     * Test of hasOverlapBox method, of class BreakDownBox.
     */
    @Test
    public void testCorrectlyResponsesToOverlap() {
        
        Box a=new Box(new Vector3dJME(0,0,0),new Vector3dJME(10,10,10));
        Box b=new Box(new Vector3dJME(0,0,0),new Vector3dJME(1,1,1));
        Box c=new Box(new Vector3dJME(1,1,1),new Vector3dJME(2,2,2)); 
        Box d=new Box(new Vector3dJME(2,2,2),new Vector3dJME(3,3,3)); 
        Box e=new Box(new Vector3dJME(3,3,3),new Vector3dJME(4,4,4)); 
        
        Collection<Box> boxes_cantAvoidOverlap=new ArrayList<>();
        boxes_cantAvoidOverlap.add(a);
        boxes_cantAvoidOverlap.add(b);
        boxes_cantAvoidOverlap.add(c);
        boxes_cantAvoidOverlap.add(d);
        boxes_cantAvoidOverlap.add(e);
        
        
        BreakDownBox bdBox=new BreakDownBox(boxes_cantAvoidOverlap,1);
        
        assertTrue(bdBox.hasOverlapBox());
        
        //overlap is only one box that had to overlap
        assertEquals(1,bdBox.getOverlapBox().getBoxesInThisAndChildren().size());
        assertTrue(bdBox.getOverlapBox().getBoxesInThisAndChildren().contains(a));
        
        /*
        * technically subbox 1 and 2 are permitted to be orientated either way so
        * we have to do some either or testing
        */
        
        Collection<Box> subBox1Boxes=bdBox.getSubBox1().getBoxesInThisAndChildren();
        Collection<Box> subBox2Boxes=bdBox.getSubBox2().getBoxesInThisAndChildren();
        
        assertTrue(
                (subBox1Boxes.contains(b) && subBox1Boxes.contains(c)) ||
                (subBox1Boxes.contains(d) && subBox1Boxes.contains(e))         
        );
        assertTrue(
                (subBox2Boxes.contains(b) && subBox2Boxes.contains(c)) ||
                (subBox2Boxes.contains(d) && subBox2Boxes.contains(e))         
        );
        
        assertEquals(2,subBox1Boxes.size());
        assertEquals(2,subBox2Boxes.size());
        
        
    }

    @Test
    public void testGetCentreOfMassFromGeometry(){
        Collection<Box> boxes=new ArrayList<>();
        boxes.add(new Box(new Vector3dJME(0,0,0),new Vector3dJME(6,4,2))); //centre of mass of 3,2,1 with weight 48
        boxes.add(new Box(new Vector3dJME(-1,-1,-1),new Vector3dJME(0,0,0))); //centre of mass of -0.5,-0.5,-0.5 with weight 1
        boxes.add(new Box(new Vector3dJME(-2,-3,-4),new Vector3dJME(0,0,0))); //centre of mass of -1,-1.5,-2 with weight 24
        
        BreakDownBox bdBox=new BreakDownBox(boxes,1);
        
        Vector3dJME expected=new Vector3dJME(
                (3*48-0.5*1-1*24)/(48+1+24),
                (2*48-0.5*1-1.5*24)/(48+1+24),
                (1*48-0.5*1-2*24)/(48+1+24)
        );
        
        Vector3dJME actual=bdBox.getCentreOfMassFromGeometry();
        
        assertEquals(expected.x,actual.x,0.0001);
        assertEquals(expected.y,actual.y,0.0001); 
        assertEquals(expected.z,actual.z,0.000001); 
    }
    
}
