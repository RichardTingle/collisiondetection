/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.collisionshape;

import blockphysics.maths.Vector3dJME;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class BoxTest {
    
    @Test
    public void testGetVolume() {
        Box b = new Box(new Vector3dJME(-5,4,6),new Vector3dJME(5,6,9));
        
        assertEquals(10*2*3,b.getVolume(),0.001);
        
    }
    
    @Test
    public void testGetCentre() {
        Box b = new Box(new Vector3dJME(-5,4,6),new Vector3dJME(5,6,9));
        
        Vector3dJME expected=new Vector3dJME(0,5,7.5);
        Vector3dJME actual=b.getCentre();
        
        assertEquals(expected.x,actual.x,0.0001);
        assertEquals(expected.y,actual.y,0.0001); 
        assertEquals(expected.z,actual.z,0.0001); 
    }
    
}
