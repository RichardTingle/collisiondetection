/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richard
 */
public class MatrixTest {
    
    public MatrixTest() {
    }



    /**
     * Test of getTranspose method, of class Matrix.
     */
    @Test
    public void testGetTranspose() {
        double[][] data =new double[3][3];
        //[ -1 2 4]
        //[ 2  3 4]
        //[ 3  4 5]
        data[0][0]=-1;
        data[0][1]=2;
        data[0][2]=4;
        data[1][0]=2;
        data[1][1]=3;
        data[1][2]=4;
        data[2][0]=3;
        data[2][1]=4;
        data[2][2]=5;
        
        Matrix startMatrix=new Matrix(data);
        
        double[][] dataExpected =new double[3][3];
        //[ -1 2 3]
        //[ 2  3 4]
        //[ 4  4 5]
        dataExpected[0][0]=-1;
        dataExpected[0][1]=2;
        dataExpected[0][2]=3;
        dataExpected[1][0]=2;
        dataExpected[1][1]=3;
        dataExpected[1][2]=4;
        dataExpected[2][0]=4;
        dataExpected[2][1]=4;
        dataExpected[2][2]=5;
        
        Matrix expectedMatrix=new Matrix(dataExpected);
        
        assertEquals(expectedMatrix,startMatrix.getTranspose());
        
    }

    /**
     * Test of findDeterminant method, of class Matrix.
     */
    @Test
    public void testFindDeterminant() throws Exception {
        
         double[][] data =new double[3][3];
        //[ -1 2 4]
        //[ 2  3 4]
        //[ 3  4 5]
        data[0][0]=-1;
        data[0][1]=2;
        data[0][2]=4;
        data[1][0]=2;
        data[1][1]=3;
        data[1][2]=4;
        data[2][0]=3;
        data[2][1]=4;
        data[2][2]=5;
        
        Matrix startMatrix=new Matrix(data);
       
        
        assertEquals(1,startMatrix.findDeterminant(),0.00001);
        
        
    }



    
}
