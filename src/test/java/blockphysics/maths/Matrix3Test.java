/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richard
 */
public class Matrix3Test {
    
    public Matrix3Test() {
    }




    /**
     * Test of multiply method, of class Matrix3.
     * @throws blockphysics.maths.InvalidMatrixSizeException
     */
    @Test
    public void testMultiply_Vector3dJME() throws InvalidMatrixSizeException {
        
        double[][] data =new double[3][3];
        //[1   2   3]
        //[10  20  30]
        //[100 200 300]
        data[0][0]=1;
        data[0][1]=2;
        data[0][2]=3;
        data[1][0]=10;
        data[1][1]=20;
        data[1][2]=30;
        data[2][0]=100;
        data[2][1]=200;
        data[2][2]=300;
        
        Matrix3 startMatrix=new Matrix3(data);
        
        Vector3dJME output=startMatrix.multiply(new Vector3dJME(1,2,3));
        
        Vector3dJME expected = new Vector3dJME(1*1 + 2*2 + 3*3, 10*1 + 20*2 + 30*3, 100*1 + 200*2 + 300*3);
        
        assertEquals(expected,output);
    }



    /**
     * Test of findInverse3By3 method, of class Matrix3.
     */
    @Test
    public void testFindInverse3By3() throws InvalidMatrixSizeException {
        
        double[][] data =new double[3][3];
        //[1   2  1]
        //[5   6  7]
        //[5   6  4]
        data[0][0]=1;
        data[0][1]=2;
        data[0][2]=1;
        data[1][0]=5;
        data[1][1]=6;
        data[1][2]=7;
        data[2][0]=5;
        data[2][1]=6;
        data[2][2]=4;
        
        Matrix3 startMatrix=new Matrix3(data);
        
        //calculated using online calculator
        double[][] dataInverse =new double[3][3];
        //[-3/2   -1/6    2/3]
        //[5/4    -1/12  -1/6]
        //[0      1/3    -1/3]
        dataInverse[0][0]= -3.0/2;
        dataInverse[0][1]= -1.0/6;
        dataInverse[0][2]= 2.0/3;
        dataInverse[1][0]= 5.0/4;
        dataInverse[1][1]= -1.0/12;
        dataInverse[1][2]= -1.0/6;
        dataInverse[2][0]= 0.0;
        dataInverse[2][1]= 1.0/3;
        dataInverse[2][2]= -1.0/3;
        Matrix3 startExpected=new Matrix3(dataInverse);
        
        Matrix3 expectedMatrix=new Matrix3(data);
        
        assertTrue(expectedMatrix.equals(startExpected.findInverse3By3(),0.0001));
    }
    
}
