/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths.sorting;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Richs
 */
public class InsertionSortTest {
    
    public InsertionSortTest() {
        
    }

    @Test
    public void testSort() {
        
        List<String> sortableStrings=new ArrayList<>();
        sortableStrings.add("eeeeeeeeeee");
        sortableStrings.add("a");
        sortableStrings.add("cccc");
        sortableStrings.add("ddddddddd");
        sortableStrings.add("bbb");
        
        SortAssessment<String> lengthBasedAssessment= (string)->string.length();
        
        
        InsertionSort<String> sorter=new InsertionSort<>();
        
        sorter.sort(sortableStrings, lengthBasedAssessment);
        
        assertEquals("a",sortableStrings.get(0));
        assertEquals("bbb",sortableStrings.get(1));       
        assertEquals("cccc",sortableStrings.get(2));     
        assertEquals("ddddddddd",sortableStrings.get(3));    
        assertEquals("eeeeeeeeeee",sortableStrings.get(4));    
        
    }
    
}
