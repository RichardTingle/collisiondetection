/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;

/**
 *
 * @author Richard
 */
public class InvalidMatrixSizeException extends Exception{
    public InvalidMatrixSizeException(){
        super();
    }

    public InvalidMatrixSizeException(String message) {
        super(message);
    }

    public InvalidMatrixSizeException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
