
package blockphysics.maths;

//a lot of processes derive from a (3 by 3) matrix, this is the basis of them all

import java.text.DecimalFormat;
import java.util.Arrays;


public class Matrix{
    
    //data[down][along]
    protected double[][] data; 
    
    protected Matrix(double[][] data){
        this.data=data;
    }
    
    @Override
    public boolean equals(Object obj){
        return equals(obj,0);
    }
    
    
    public boolean equals(Object obj, double permittedError){
        if (obj instanceof Matrix){
            Matrix other=(Matrix)obj;
            if (data.length==other.data.length && data[0].length==other.data[0].length ){
                for(int i=0;i<data.length;i++){
                    for(int j=0;j<data.length;j++){
                        if (Math.abs(data[i][j]-other.data[i][j])>permittedError){
                            return false;
                        }
                    }
                }
                return true;
            }else{
                return false;
            }
                    
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Arrays.deepHashCode(this.data);
        return hash;
    }
    
    public double getElement(int x,int y){
        return data[x][y];
    }
    
    protected Matrix multiply(Matrix other) throws InvalidMatrixSizeException{
        //ensure valid multiplication (inner dimentions must be equal
        if (this.data[0].length==other.data.length){
            double[][] newData=new double[this.data.length][other.data[0].length];
            
            for(int i=0;i<newData.length;i++){ //the down
                for(int j=0;j<newData[0].length;j++){ //the along
                    newData[i][j]=0;
                    for(int k=0;k<this.data[0].length;k++){
                        newData[i][j]+=this.data[i][k]*other.data[k][j];
                    }
                    
                }
            }
            
            if (newData.length==3 && newData[0].length==3){
                return new Matrix3(newData);
            }
            
            return new Matrix(newData);
        }else{
            throw new InvalidMatrixSizeException();
        }
    }
    

    public Matrix getTranspose(){
        //checked, is correct

        double newContents[][]=new double[this.data[0].length][this.data.length];
        
        for(int i=0;i<this.data.length;i++){
            for(int j=0;j<this.data[0].length;j++){
                newContents[j][i]=this.data[i][j];
            }
        }
        return new Matrix(newContents);
        
    }
    
    public double findDeterminant() throws InvalidMatrixSizeException{
        return findDeterminantRecursive(this);
    }
    
    private double findDeterminantRecursive(Matrix subMatrix) throws InvalidMatrixSizeException{
        //check dimentions are equal (otherwise the determinant is undefined)
        double output=0;
        if (subMatrix.data.length==subMatrix.data[0].length){
            if (subMatrix.data.length==2){ //if its a 2*2 matrix its easy, otherwise break apart and call reciprocally until broken down to 2*2s
                output=((subMatrix.data[0][0]*subMatrix.data[1][1])-((subMatrix.data[1][0]* subMatrix.data[0][1])));
            }else{
                for(int i=0;i<subMatrix.data.length;i++){
                    //build subsubMatrix to be passed back recursively
                    double [][] subsubMatrixBuild=new double[subMatrix.data.length-1][subMatrix.data.length-1];
                    
                    for(int ii=0;ii<subMatrix.data.length;ii++){ //ii moves through full matrix again so the bits for the sub matrix can be found
                        //the below takes columns other than i=ii, so creates the submatrix, the top row is also ommited
                        if (ii<i){
                            for (int j=0;j<subMatrix.data.length-1;j++){
                                subsubMatrixBuild[ii][j]=subMatrix.data[ii][j+1];
                            }
                        }
                        if (ii>i){
                            for (int j=0;j<subMatrix.data.length-1;j++){
                                subsubMatrixBuild[ii-1][j]=subMatrix.data[ii][j+1];
                            }                            
                        }
                                
                    }
                    
                    Matrix subsubMatrix=new Matrix(subsubMatrixBuild);
                    
                    if (i % 2 == 0){//is Even (NOTE zero is first entry)

                        output=output+subMatrix.data[i][0]*findDeterminantRecursive(subsubMatrix);

                            
                    }else{//is odd
                        output=output-(subMatrix.data[i][0]*findDeterminantRecursive(subsubMatrix));
                    }
                }
                
            }
            
            
        }else{
            throw new InvalidMatrixSizeException();
        }
        
        return output;
    }
    
    public void scaleLocal(double scale){
        for(int i=0;i<this.data.length;i++){
            for(int j=0;j<this.data[0].length;j++){
                data[i][j]=data[i][j]*scale;
            }
        }
    }
    
    
    public void addLocal(Matrix other){
        //check dimentions
        
        if (this.data.length!=other.data.length || this.data[0].length!=other.data[0].length){
            throw new RuntimeException("adding matricies of different sizes");
        } else{
            for(int i=0;i<this.data.length;i++){
                for(int j=0;j<this.data[0].length;j++){
                    data[i][j]+=other.data[i][j];
                }
            }
        }
    }
    
    public boolean isNaN(){
        for(int i=0;i<this.data.length;i++){
            for(int j=0;j<this.data[0].length;j++){
                if (Double.isNaN(data[i][j])){
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public String toString(){
        //data[down][along]
        
        String outString="";
        for(int i=0;i<data.length;i++){
            outString+="[";
            for(int j=0;j<data.length;j++){
                
                outString+=data[i][j];
                
                
                if (j!=data.length-1){
                    outString+=",";
                }
            }
            outString+="]";
        }
        return outString;
    }
}
