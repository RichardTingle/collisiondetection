/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths.sorting;

/**
 * Converts a complex objects to a numeric value which is then used by the 
 * sorting algorithm
 * @author Richs
 * @param <T> The type of object being assessed
 */
@FunctionalInterface 
public interface SortAssessment<T> {
    public long assess(T t);
}
