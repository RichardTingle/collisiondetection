/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths.sorting;

import java.util.List;

/**
 *
 * @author Richard
 * @param <T>
 */
public class InsertionSort<T> {
    
    /**
     * Rearranges the list such that it is ascending order as assessed by the 
     * assessmentAlgorithm
     * @param list
     * @param assessmentAlgorithm 
     */
    public void sort(List<T> list, SortAssessment<T> assessmentAlgorithm){
        //insertion sort is highly eficient for nearly sorted lists
        //http://en.wikipedia.org/wiki/Insertion_sort

        
        long[] numericEquivalent=new long[list.size()];
        
        for(int i=0;i<list.size();i++){
            numericEquivalent[i]=assessmentAlgorithm.assess(list.get(i));
        }
        
        //sort from small to large
        for(int i=1;i<list.size();i++){
            T workingItem=list.get(i);
            long workingAssessment=numericEquivalent[i];
            
            int j=i-1;
            
            while(j>=0 && numericEquivalent[j] > workingAssessment){
                
                numericEquivalent[j+1]=numericEquivalent[j];
                list.set(j+1, list.get(j));
                
                j=j-1;
            }
            
            numericEquivalent[j+1]=workingAssessment;
            list.set(j+1,workingItem);
        }
    }
    
}
