/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Richard
 */
public class Matrix3 extends Matrix{

    public static Matrix3 ZERO=new Matrix3();
    
    public Matrix3(){
        super(new double[3][3]);
    }
    
    public Matrix3(double[][] data) throws InvalidMatrixSizeException {
        super(data);
        
        if (data.length!=3 || data[0].length!=3 ){
            throw new InvalidMatrixSizeException();
        }
    }
    
    public Matrix3(Matrix matrix) throws InvalidMatrixSizeException {
        super(matrix.data);
        
        if (data.length!=3 || data[0].length!=3 ){
            throw new InvalidMatrixSizeException();
        }
    }
    
    public Matrix3 add(Matrix matrix){
        Matrix3 returnValue=null;
        try {
            returnValue=new Matrix3(this);
        } catch (InvalidMatrixSizeException ex) {
            throw new RuntimeException("A matrix3 wasn't 3 by 3, somehow!");
        }
        returnValue.addLocal(matrix);
        return returnValue;
    }
    
    public void setSkewSymmetric(Vector3dJME vector){
        //this is a little complex but basically this creates a 3 by 3 matrix 
        //"equivalent" of the input vector (bear with me), such that multiplication
        //of two such matricies is the equivelent of a cross product. 
        //Well see pg 404 of Game physics engine development anyway
        
        //data[down][along]
        
        data[0][0]=0;
        data[1][0]=vector.z;
        data[2][0]=-vector.y;
        
        data[0][1]=-vector.z;
        data[1][1]=0;
        data[2][1]=vector.x;
        
        data[0][2]=vector.y;
        data[1][2]=-vector.x;
        data[2][2]=0;
        
    }
    
    public void setDiagonal(Vector3dJME vector){
        //this sets the matrix with zeros off diagonal and the vector inputs on diagonal
        
        //data[down][along]
        
        data[0][0]=vector.x;
        data[1][0]=0;
        data[2][0]=0;
        
        data[0][1]=0;
        data[1][1]=vector.y;
        data[2][1]=0;
        
        data[0][2]=0;
        data[1][2]=0;
        data[2][2]=vector.z;
        
    }
    
    
    
    @Override
    public Matrix3 getTranspose(){
        try {
            return new Matrix3(super.getTranspose());
        } catch (InvalidMatrixSizeException ex) {
            //should never happen because this is garanteed to already be a 3 by 3
            throw new RuntimeException("Matrix3 transpose was not a matrix3 (somehow)");
        }
    }
    

    
    public Vector3dJME multiply(Vector3dJME in){

       
        double x=data[0][0]*in.x+data[0][1]*in.y+data[0][2]*in.z;
        double y=data[1][0]*in.x+data[1][1]*in.y+data[1][2]*in.z;
        double z=data[2][0]*in.x+data[2][1]*in.y+data[2][2]*in.z;
        
        return new Vector3dJME(x,y,z);

        
    }
    
    public Matrix3 multiply(Matrix3 in){
        try {
            //convert to the matric representation (column vector)

            Matrix outMatrix=super.multiply(in);
            
            return new Matrix3(outMatrix);
        } catch (InvalidMatrixSizeException ex) {
            //should never happen
            throw new RuntimeException("Matrix invalid");
        }
        
    }
    
    @Override
    public double findDeterminant(){
        //special unrolled 3 by 3 version, it looks horrible but is built by 
        //a CodeWriter (see CodeWriter project)
        //I promise this was a bottle neck and it did speed it up by *4
        return +data[0][0]*(data[1][1]*data[2][2]-data[1][2]*data[2][1])-data[0][1]*(data[1][0]*data[2][2]-data[1][2]*data[2][0])+data[0][2]*(data[1][0]*data[2][1]-data[1][1]*data[2][0]);
    }
     
    
    public Matrix3 findInverse3By3(){
        //see http://www.wikihow.com/Inverse-a-3X3-Matrix
        //checked and working
        
        if (this.data.length==this.data[0].length && this.data.length==3){
            double[][] returnBuild=new double[this.data.length][this.data.length];
             
            double prefactor;

                prefactor = 1.0/findDeterminant();

             Matrix transpose=this.getTranspose();
             
             
             for(int along=0;along<3;along++){ //i is along
                 for(int down=0;down<3;down++){ //
                     //each element is the determinant of the remaining 2 by 2
                     //and a + - +
                     //      - + -
                     //      + - +
                     //patern
                     
                     double premultiplyer;
                     
                     if ((along%2==0 && down%2==0)||(along%2==1 && down%2==1)){ //both even or both odd
                         premultiplyer=prefactor;
                     }else{
                         premultiplyer=-prefactor;
                     }
                     
                     //subMatrix
                     double subMatrixBuild[][]=new double[2][2];
                     
                     int acrosses[]=new int[2];
                     int downs[]=new int[2];
                     
                     int j=0;
                     int k=0;
                             
                     for(int i=0;i<3;i++){
                         if (i!=along){
                             acrosses[j]=i;
                             j++;
                         }
                         if (i!=down){
                             downs[k]=i;
                             k++;
                         }
                     }
                     
                     for(int ac=0;ac<2;ac++){
                         for (int dr=0;dr<2;dr++){
                             subMatrixBuild[dr][ac]=transpose.data [downs[dr]][acrosses[ac]];
                         }
                     }
                     
                     Matrix subMatrix=new Matrix(subMatrixBuild);
                    try {
                        returnBuild[down][along]=premultiplyer*subMatrix.findDeterminant();
                    } catch (InvalidMatrixSizeException ex) {
                        throw new RuntimeException("Incorrect matrix size"); //should be impossible
                    }
                     
                     
                 }
             }
            try {
                Matrix3 returnMatrix=new Matrix3(returnBuild);

                return returnMatrix;
            } catch (InvalidMatrixSizeException ex) {
                throw new RuntimeException("Incorrect matrix size"); //should be impossible
            }
        }else{
            return null;
        }

    }
    

    
}
