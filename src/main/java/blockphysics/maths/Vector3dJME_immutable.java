/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;


/**
 *Serves sole purpose of creating Vector3dJMEs that can't be changed
 * @author Richs
 */
public class Vector3dJME_immutable extends Vector3dJME{

    public Vector3dJME_immutable() {
    }

    public Vector3dJME_immutable(double x, double y, double z) {
        super(x, y, z);
    }

    public Vector3dJME_immutable(Vector3dJME copy) {
        super(copy);
    }



    @Override
    public Vector3dJME set(Vector3dJME vect) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME addLocal(Vector3dJME vec) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME addLocal(double addX, double addY, double addZ) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME crossLocal(Vector3dJME v) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME crossLocal(double otherX, double otherY, double otherZ) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME multLocal(double scalar) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME multLocal(Vector3dJME vec) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME multLocal(double x, double y, double z) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME divideLocal(double scalar) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME divideLocal(Vector3dJME scalar) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME negateLocal() {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME subtractLocal(Vector3dJME vec) {
       throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME subtractLocal(double subtractX, double subtractY, double subtractZ) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME normalizeLocal() {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public void maxLocal(Vector3dJME other) {
       throw new RuntimeException("Vector is immutable");
    }

    @Override
    public void minLocal(Vector3dJME other) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME setX(double x) {
        throw new RuntimeException("Vector is immutable");
    }

    @Override
    public Vector3dJME setY(double y) {
        return super.setY(y); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void set(int index, double value) {
        throw new RuntimeException("Vector is immutable");
    }

    
    
}
