


package blockphysics.maths;

import java.io.IOException;
import java.util.logging.Logger;


//this class mimics Vector3f but with double precision

public class Vector3dJME implements Cloneable, java.io.Serializable {

    static final long serialVersionUID = 1;
    
    private static final Logger logger = Logger.getLogger(Vector3dJME.class.getName());

    public final static Vector3dJME ZERO = new Vector3dJME_immutable(0, 0, 0);
    public final static Vector3dJME NAN = new Vector3dJME_immutable(Double.NaN, Double.NaN, Double.NaN);
    public final static Vector3dJME UNIT_X = new Vector3dJME_immutable(1, 0, 0);
    public final static Vector3dJME UNIT_Y = new Vector3dJME_immutable(0, 1, 0);
    public final static Vector3dJME UNIT_Z = new Vector3dJME_immutable(0, 0, 1);
    public final static Vector3dJME UNIT_XYZ = new Vector3dJME_immutable(1, 1, 1);
    public final static Vector3dJME POSITIVE_INFINITY = new Vector3dJME_immutable(
            Double.POSITIVE_INFINITY,
            Double.POSITIVE_INFINITY,
            Double.POSITIVE_INFINITY);
    public final static Vector3dJME NEGATIVE_INFINITY = new Vector3dJME_immutable(
            Double.NEGATIVE_INFINITY,
            Double.NEGATIVE_INFINITY,
            Double.NEGATIVE_INFINITY);
    public static Vector3dJME HALF=new Vector3dJME_immutable(0.5, 0.5, 0.5); //provided to be reused

    public static Vector3dJME average(Vector3dJME minimum, Vector3dJME maximum) {
        return new Vector3dJME((minimum.x+maximum.x)/2,(minimum.y+maximum.y)/2,(minimum.z+maximum.z)/2);
    }

    /**
     * Changes the values such that they are actually the minimum and maximum
     * @param sourceWindowMininum_chunkLocal
     * @param sourceWindowMaxinum_chunkLocal 
     */
    public static void reshuffleToMinMaxForm(Vector3dJME desiredMin, Vector3dJME desiredMax) {
        for(int axis=0;axis<3;axis++){
            if (desiredMin.get(axis)>desiredMax.get(axis)){
                double temp=desiredMin.get(axis);
                desiredMin.set(axis, desiredMax.get(axis));
                desiredMax.set(axis, temp);
            }
        }
    }

    
	/**
     * the x value of the vector.
     */
    public double x;

    /**
     * the y value of the vector.
     */
    public double y;

    /**
     * the z value of the vector.
     */
    public double z;

    /**
     * Constructor instantiates a new <code>Vector3dJME</code> with default
     * values of (0,0,0).
     *
     */
    public Vector3dJME() {
        x = y = z = 0;
    }

    /**
     * Constructor instantiates a new <code>Vector3dJME</code> with provides
     * values.
     *
     * @param x
     *            the x value of the vector.
     * @param y
     *            the y value of the vector.
     * @param z
     *            the z value of the vector.
     */
    public Vector3dJME(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    


    /**
     * Constructor instantiates a new <code>Vector3dJME</code> that is a copy
     * of the provided vector
     * @param copy The Vector3dJME to copy
     */
    public Vector3dJME(Vector3dJME copy) {
        this.set(copy);
    }



    
    /**
     * <code>set</code> sets the x,y,z values of the vector based on passed
     * parameters.
     *
     * @param x
     *            the x value of the vector.
     * @param y
     *            the y value of the vector.
     * @param z
     *            the z value of the vector.
     * @return this vector
     */
    public Vector3dJME set(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    /**
     * <code>set</code> sets the x,y,z values of the vector by copying the
     * supplied vector.
     *
     * @param vect
     *            the vector to copy.
     * @return this vector
     */
    public Vector3dJME set(Vector3dJME vect) {
        this.x = vect.x;
        this.y = vect.y;
        this.z = vect.z;
        return this;
    }

    /**
     *
     * <code>add</code> adds a provided vector to this vector creating a
     * resultant vector which is returned. If the provided vector is null, null
     * is returned.
     *
     * @param vec
     *            the vector to add to this.
     * @return the resultant vector.
     */
    public Vector3dJME add(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        return new Vector3dJME(x + vec.x, y + vec.y, z + vec.z);
    }

    /**
     *
     * <code>add</code> adds the values of a provided vector storing the
     * values in the supplied vector.
     *
     * @param vec
     *            the vector to add to this
     * @param result
     *            the vector to store the result in
     * @return result returns the supplied result vector.
     */
    public Vector3dJME add(Vector3dJME vec, Vector3dJME result) {
        result.x = x + vec.x;
        result.y = y + vec.y;
        result.z = z + vec.z;
        return result;
    }

    /**
     * <code>addLocal</code> adds a provided vector to this vector internally,
     * and returns a handle to this vector for easy chaining of calls. If the
     * provided vector is null, null is returned.
     *
     * @param vec
     *            the vector to add to this vector.
     * @return this
     */
    public Vector3dJME addLocal(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        x += vec.x;
        y += vec.y;
        z += vec.z;
        return this;
    }

    /**
     *
     * <code>add</code> adds the provided values to this vector, creating a
     * new vector that is then returned.
     *
     * @param addX
     *            the x value to add.
     * @param addY
     *            the y value to add.
     * @param addZ
     *            the z value to add.
     * @return the result vector.
     */
    public Vector3dJME add(double addX, double addY, double addZ) {
        return new Vector3dJME(x + addX, y + addY, z + addZ);
    }

    /**
     * <code>addLocal</code> adds the provided values to this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls.
     *
     * @param addX
     *            value to add to x
     * @param addY
     *            value to add to y
     * @param addZ
     *            value to add to z
     * @return this
     */
    public Vector3dJME addLocal(double addX, double addY, double addZ) {
        x += addX;
        y += addY;
        z += addZ;
        return this;
    }

    /**
     *
     * <code>scaleAdd</code> multiplies this vector by a scalar then adds the
     * given Vector3dJME.
     *
     * @param scalar
     *            the value to multiply this vector by.
     * @param add
     *            the value to add
     */
    public Vector3dJME scaleAdd(double scalar, Vector3dJME add) {
        x = x * scalar + add.x;
        y = y * scalar + add.y;
        z = z * scalar + add.z;
        return this;
    }

    /**
     *
     * <code>scaleAdd</code> multiplies the given vector by a scalar then adds
     * the given vector.
     *
     * @param scalar
     *            the value to multiply this vector by.
     * @param mult
     *            the value to multiply the scalar by
     * @param add
     *            the value to add
     */
    public Vector3dJME scaleAdd(double scalar, Vector3dJME mult, Vector3dJME add) {
        this.x = mult.x * scalar + add.x;
        this.y = mult.y * scalar + add.y;
        this.z = mult.z * scalar + add.z;
        return this;
    }

    /**
     *
     * <code>dot</code> calculates the dot product of this vector with a
     * provided vector. If the provided vector is null, 0 is returned.
     *
     * @param vec
     *            the vector to dot with this vector.
     * @return the resultant dot product of this vector and a given vector.
     */
    public double dot(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, 0 returned.");
            return 0;
        }
        return x * vec.x + y * vec.y + z * vec.z;
    }

    /**
     * <code>cross</code> calculates the cross product of this vector with a
     * parameter vector v.
     *
     * @param v
     *            the vector to take the cross product of with this.
     * @return the cross product vector.
     */
    public Vector3dJME cross(Vector3dJME v) {
        return cross(v, null);
    }

    /**
     * <code>cross</code> calculates the cross product of this vector with a
     * parameter vector v.  The result is stored in <code>result</code>
     *
     * @param v
     *            the vector to take the cross product of with this.
     * @param result
     *            the vector to store the cross product result.
     * @return result, after recieving the cross product vector.
     */
    public Vector3dJME cross(Vector3dJME v,Vector3dJME result) {
        return cross(v.x, v.y, v.z, result);
    }

    /**
     * <code>cross</code> calculates the cross product of this vector with a
     * parameter vector v.  The result is stored in <code>result</code>
     *
     * @param otherX
     *            x component of the vector to take the cross product of with this.
     * @param otherY
     *            y component of the vector to take the cross product of with this.
     * @param otherZ
     *            z component of the vector to take the cross product of with this.
     * @param result
     *            the vector to store the cross product result.
     * @return result, after recieving the cross product vector.
     */
    public Vector3dJME cross(double otherX, double otherY, double otherZ, Vector3dJME result) {
        if (result == null) result = new Vector3dJME();
        double resX = ((y * otherZ) - (z * otherY)); 
        double resY = ((z * otherX) - (x * otherZ));
        double resZ = ((x * otherY) - (y * otherX));
        result.set(resX, resY, resZ);
        return result;
    }

    /**
     * <code>crossLocal</code> calculates the cross product of this vector
     * with a parameter vector v.
     *
     * @param v
     *            the vector to take the cross product of with this.
     * @return this.
     */
    public Vector3dJME crossLocal(Vector3dJME v) {
        return crossLocal(v.x, v.y, v.z);
    }

    /**
     * <code>crossLocal</code> calculates the cross product of this vector
     * with a parameter vector v.
     *
     * @param otherX
     *            x component of the vector to take the cross product of with this.
     * @param otherY
     *            y component of the vector to take the cross product of with this.
     * @param otherZ
     *            z component of the vector to take the cross product of with this.
     * @return this.
     */
    public Vector3dJME crossLocal(double otherX, double otherY, double otherZ) {
        double tempx = ( y * otherZ ) - ( z * otherY );
        double tempy = ( z * otherX ) - ( x * otherZ );
        z = (x * otherY) - (y * otherX);
        x = tempx;
        y = tempy;
        return this;
    }

    public Vector3dJME project(Vector3dJME other){
        double n = this.dot(other); // A . B
        double d = other.lengthSquared(); // |B|^2
        return new Vector3dJME(other).normalizeLocal().multLocal(n/d);
    }

    /**
     * Returns true if this vector is a unit vector (length() ~= 1),
     * returns false otherwise.
     * 
     * @return true if this vector is a unit vector (length() ~= 1),
     * or false otherwise.
     */
    public boolean isUnitVector(){
        double len = length();
        return 0.99f < len && len < 1.01f;
    }

    /**
     * <code>length</code> calculates the magnitude of this vector.
     *
     * @return the length or magnitude of the vector.
     */
    public double length() {
        return Math.sqrt(lengthSquared());
    }

    /**
     * <code>lengthSquared</code> calculates the squared value of the
     * magnitude of the vector.
     *
     * @return the magnitude squared of the vector.
     */
    public double lengthSquared() {
        return x * x + y * y + z * z;
    }

    /**
     * <code>distanceSquared</code> calculates the distance squared between
     * this vector and vector v.
     *
     * @param v the second vector to determine the distance squared.
     * @return the distance squared between the two vectors.
     */
    public double distanceSquared(Vector3dJME v) {
        double dx = x - v.x;
        double dy = y - v.y;
        double dz = z - v.z;
        return (double) (dx * dx + dy * dy + dz * dz);
    }

    /**
     * <code>distance</code> calculates the distance between this vector and
     * vector v.
     *
     * @param v the second vector to determine the distance.
     * @return the distance between the two vectors.
     */
    public double distance(Vector3dJME v) {
        return Math.sqrt(distanceSquared(v));
    }

    /**
     *
     * <code>mult</code> multiplies this vector by a scalar. The resultant
     * vector is returned.
     *
     * @param scalar
     *            the value to multiply this vector by.
     * @return the new vector.
     */
    public Vector3dJME mult(double scalar) {
        return new Vector3dJME(x * scalar, y * scalar, z * scalar);
    }

    /**
     *
     * <code>mult</code> multiplies this vector by a scalar. The resultant
     * vector is supplied as the second parameter and returned.
     *
     * @param scalar the scalar to multiply this vector by.
     * @param product the product to store the result in.
     * @return product
     */
    public Vector3dJME mult(double scalar, Vector3dJME product) {
        if (null == product) {
            product = new Vector3dJME();
        }

        product.x = x * scalar;
        product.y = y * scalar;
        product.z = z * scalar;
        return product;
    }

    /**
     * <code>multLocal</code> multiplies this vector by a scalar internally,
     * and returns a handle to this vector for easy chaining of calls.
     *
     * @param scalar
     *            the value to multiply this vector by.
     * @return this
     */
    public Vector3dJME multLocal(double scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return this;
    }

    /**
     * <code>multLocal</code> multiplies a provided vector to this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls. If the provided vector is null, null is returned.
     *
     * @param vec
     *            the vector to mult to this vector.
     * @return this
     */
    public Vector3dJME multLocal(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        x *= vec.x;
        y *= vec.y;
        z *= vec.z;
        return this;
    }

    /**
     * <code>multLocal</code> multiplies this vector by 3 scalars
     * internally, and returns a handle to this vector for easy chaining of
     * calls.
     *
     * @param x
     * @param y
     * @param z
     * @return this
     */
    public Vector3dJME multLocal(double x, double y, double z) {
        this.x *= x;
        this.y *= y;
        this.z *= z;
        return this;
    }

    /**
     * <code>multLocal</code> multiplies a provided vector to this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls. If the provided vector is null, null is returned.
     *
     * @param vec
     *            the vector to mult to this vector.
     * @return this
     */
    public Vector3dJME mult(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        return mult(vec, null);
    }

    /**
     * <code>multLocal</code> multiplies a provided vector to this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls. If the provided vector is null, null is returned.
     *
     * @param vec
     *            the vector to mult to this vector.
     * @param store result vector (null to create a new vector)
     * @return this
     */
    public Vector3dJME mult(Vector3dJME vec, Vector3dJME store) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        if (store == null) store = new Vector3dJME();
        return store.set(x * vec.x, y * vec.y, z * vec.z);
    }


    /**
     * <code>divide</code> divides the values of this vector by a scalar and
     * returns the result. The values of this vector remain untouched.
     *
     * @param scalar
     *            the value to divide this vectors attributes by.
     * @return the result <code>Vector</code>.
     */
    public Vector3dJME divide(double scalar) {
        scalar = 1f/scalar;
        return new Vector3dJME(x * scalar, y * scalar, z * scalar);
    }

    /**
     * <code>divideLocal</code> divides this vector by a scalar internally,
     * and returns a handle to this vector for easy chaining of calls. Dividing
     * by zero will result in an exception.
     *
     * @param scalar
     *            the value to divides this vector by.
     * @return this
     */
    public Vector3dJME divideLocal(double scalar) {
        scalar = 1f/scalar;
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return this;
    }


    /**
     * <code>divide</code> divides the values of this vector by a scalar and
     * returns the result. The values of this vector remain untouched.
     *
     * @param scalar
     *            the value to divide this vectors attributes by.
     * @return the result <code>Vector</code>.
     */
    public Vector3dJME divide(Vector3dJME scalar) {
        return new Vector3dJME(x / scalar.x, y / scalar.y, z / scalar.z);
    }

    /**
     * <code>divideLocal</code> divides this vector by a scalar internally,
     * and returns a handle to this vector for easy chaining of calls. Dividing
     * by zero will result in an exception.
     *
     * @param scalar
     *            the value to divides this vector by.
     * @return this
     */
    public Vector3dJME divideLocal(Vector3dJME scalar) {
        x /= scalar.x;
        y /= scalar.y;
        z /= scalar.z;
        return this;
    }

    /**
     *
     * <code>negate</code> returns the negative of this vector. All values are
     * negated and set to a new vector.
     *
     * @return the negated vector.
     */
    public Vector3dJME negate() {
        return new Vector3dJME(-x, -y, -z);
    }

    /**
     *
     * <code>negateLocal</code> negates the internal values of this vector.
     *
     * @return this.
     */
    public Vector3dJME negateLocal() {
        x = -x;
        y = -y;
        z = -z;
        return this;
    }

    /**
     *
     * <code>subtract</code> subtracts the values of a given vector from those
     * of this vector creating a new vector object. If the provided vector is
     * null, null is returned.
     *
     * @param vec
     *            the vector to subtract from this vector.
     * @return the result vector.
     */
    public Vector3dJME subtract(Vector3dJME vec) {
        return new Vector3dJME(x - vec.x, y - vec.y, z - vec.z);
    }

    /**
     * <code>subtractLocal</code> subtracts a provided vector to this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls. If the provided vector is null, null is returned.
     *
     * @param vec
     *            the vector to subtract
     * @return this
     */
    public Vector3dJME subtractLocal(Vector3dJME vec) {
        if (null == vec) {
            logger.warning("Provided vector is null, null returned.");
            return null;
        }
        x -= vec.x;
        y -= vec.y;
        z -= vec.z;
        return this;
    }

    /**
     *
     * <code>subtract</code>
     *
     * @param vec
     *            the vector to subtract from this
     * @param result
     *            the vector to store the result in
     * @return result
     */
    public Vector3dJME subtract(Vector3dJME vec, Vector3dJME result) {
        if(result == null) {
            result = new Vector3dJME();
        }
        result.x = x - vec.x;
        result.y = y - vec.y;
        result.z = z - vec.z;
        return result;
    }

    /**
     *
     * <code>subtract</code> subtracts the provided values from this vector,
     * creating a new vector that is then returned.
     *
     * @param subtractX
     *            the x value to subtract.
     * @param subtractY
     *            the y value to subtract.
     * @param subtractZ
     *            the z value to subtract.
     * @return the result vector.
     */
    public Vector3dJME subtract(double subtractX, double subtractY, double subtractZ) {
        return new Vector3dJME(x - subtractX, y - subtractY, z - subtractZ);
    }

    /**
     * <code>subtractLocal</code> subtracts the provided values from this vector
     * internally, and returns a handle to this vector for easy chaining of
     * calls.
     *
     * @param subtractX
     *            the x value to subtract.
     * @param subtractY
     *            the y value to subtract.
     * @param subtractZ
     *            the z value to subtract.
     * @return this
     */
    public Vector3dJME subtractLocal(double subtractX, double subtractY, double subtractZ) {
        x -= subtractX;
        y -= subtractY;
        z -= subtractZ;
        return this;
    }

    /**
     * <code>normalize</code> returns the unit vector of this vector.
     *
     * @return unit vector of this vector.
     */
    public Vector3dJME normalize() {
//        double length = length();
//        if (length != 0) {
//            return divide(length);
//        }
//
//        return divide(1);
        double length = x * x + y * y + z * z;
        if (length != 1f && length != 0f){
            length = 1.0f / Math.sqrt(length);
            return new Vector3dJME(x * length, y * length, z * length);
        }
        return clone();
    }

    /**
     * <code>normalizeLocal</code> makes this vector into a unit vector of
     * itself.
     *
     * @return this.
     */
    public Vector3dJME normalizeLocal() {
        // NOTE: this implementation is more optimized
        // than the old jme normalize as this method
        // is commonly used.
        double length = x * x + y * y + z * z;
        if (length != 1f && length != 0f){
            length = 1.0f / Math.sqrt(length);
            x *= length;
            y *= length;
            z *= length;
        }
        return this;
    }

    /**
     * <code>maxLocal</code> computes the maximum value for each 
     * component in this and <code>other</code> vector. The result is stored
     * in this vector.
     * @param other 
     */
    public void maxLocal(Vector3dJME other){
        x = other.x > x ? other.x : x;
        y = other.y > y ? other.y : y;
        z = other.z > z ? other.z : z;
    }

    /**
     * <code>minLocal</code> computes the minimum value for each
     * component in this and <code>other</code> vector. The result is stored
     * in this vector.
     * @param other
     */
    public void minLocal(Vector3dJME other){
        x = other.x < x ? other.x : x;
        y = other.y < y ? other.y : y;
        z = other.z < z ? other.z : z;
    }

    /**
     * <code>zero</code> resets this vector's data to zero internally.
     */
    public Vector3dJME zero() {
        x = y = z = 0;
        return this;
    }

    /**
     * Returns vector3dJME(x%remainder,y%remainder, z%remainder)
     * @param remainder
     * @return 
     */
    public Vector3dJME remainder(double remainder){
        Vector3dJME remainderValue=new Vector3dJME();
        for(int i=0;i<3;i++){
            remainderValue.set(i, remainderValue.get(i)%remainder);
        }
        return  remainderValue;
    }
    
    /**
     * Returns a Vector3dJME where each coordinate is independantly rounded to
     * the closest integer. This uses Math.rint to calculate.
     * @param remainder
     * @return 
     */
    public Vector3dJME roundToClosest(){
        Vector3dJME remainderValue=new Vector3dJME();
        for(int i=0;i<3;i++){
            remainderValue.set(i, Math.rint(this.get(i)));
        }
        return remainderValue;
    }
    
    /**
     * <code>angleBetween</code> returns (in radians) the angle between two vectors.
     * It is assumed that both this vector and the given vector are unit vectors (iow, normalized).
     * 
     * @param otherVector a unit vector to find the angle against
     * @return the angle in radians.
     */
    public double angleBetween(Vector3dJME otherVector) {
        double dotProduct = dot(otherVector);
        double angle = Math.acos(dotProduct);
        return angle;
    }
    
        /**
     * <code>angleBetween_signed</code> returns (in radians) the angle between two vectors.
     * It is assumed that both this vector and the given vector are unit vectors (iow, normalized).
     * 
     * It returns positive angles where this.dot(other) is greater that 1 and negative angles when
     * this.cross(other) is less than 1
     * 
     * @param otherVector a unit vector to find the angle against
     * @return the angle in radians.
     */
    /*public double angleBetween_signed(Vector3dJME otherVector) {
        Vector3dJME planeNormal=this.cross(otherVector);
        
        Vector3dJME vector3=planeNormal.cross(this);
        
        double signDeterminer=vector3.dot(otherVector);
        
        if (signDeterminer>0){
            return angleBetween(otherVector);
        }else{
            return -angleBetween(otherVector);
        }
    }
    */
    
    /**
     * Sets this vector to the interpolation by changeAmnt from this to the finalVec
     * this=(1-changeAmnt)*this + changeAmnt * finalVec
     * @param finalVec The final vector to interpolate towards
     * @param changeAmnt An amount between 0.0 - 1.0 representing a precentage
     *  change from this towards finalVec
     */
    public Vector3dJME interpolate(Vector3dJME finalVec, double changeAmnt) {
        this.x=(1-changeAmnt)*this.x + changeAmnt*finalVec.x;
        this.y=(1-changeAmnt)*this.y + changeAmnt*finalVec.y;
        this.z=(1-changeAmnt)*this.z + changeAmnt*finalVec.z;
        return this;
    }

    /**
     * Sets this vector to the interpolation by changeAmnt from beginVec to finalVec
     * this=(1-changeAmnt)*beginVec + changeAmnt * finalVec
     * @param beginVec the beging vector (changeAmnt=0)
     * @param finalVec The final vector to interpolate towards
     * @param changeAmnt An amount between 0.0 - 1.0 representing a precentage
     *  change from beginVec towards finalVec
     */
    public Vector3dJME interpolate(Vector3dJME beginVec,Vector3dJME finalVec, double changeAmnt) {
        this.x=(1-changeAmnt)*beginVec.x + changeAmnt*finalVec.x;
        this.y=(1-changeAmnt)*beginVec.y + changeAmnt*finalVec.y;
        this.z=(1-changeAmnt)*beginVec.z + changeAmnt*finalVec.z;
        return this;
    }

    /**
     * Check a vector... if it is null or its doubles are NaN or infinite,
     * return false.  Else return true.
     * @param vector the vector to check
     * @return true or false as stated above.
     */
    public static boolean isValidVector(Vector3dJME vector) {
      if (vector == null) return false;
      if (Double.isNaN(vector.x) ||
          Double.isNaN(vector.y) ||
          Double.isNaN(vector.z)) return false;
      if (Double.isInfinite(vector.x) ||
          Double.isInfinite(vector.y) ||
          Double.isInfinite(vector.z)) return false;
      return true;
    }

    public boolean isExclusiveGreaterThan(Vector3dJME other){
        return 
            this.x>other.x &&
            this.y>other.y &&
            this.z>other.z;  
    }

    public boolean isExclusiveLessThan(Vector3dJME other){
        return 
            this.x<other.x &&
            this.y<other.y &&
            this.z<other.z;  
    }
    
    @Override
    public Vector3dJME clone() {
        try {
            return (Vector3dJME) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(); // can not happen
        }
    }

    /**
     * Saves this Vector3dJME into the given double[] object.
     * 
     * @param doubles
     *            The double[] to take this Vector3dJME. If null, a new double[3] is
     *            created.
     * @return The array, with X, Y, Z double values in that order
     */
    public double[] toArray(double[] doubles) {
        if (doubles == null) {
            doubles = new double[3];
        }
        doubles[0] = x;
        doubles[1] = y;
        doubles[2] = z;
        return doubles;
    }

    
    public boolean equals(Object o, double permittedError) {
        if (!(o instanceof Vector3dJME)) { return false; }

        if (this == o) { return true; }

        Vector3dJME comp = (Vector3dJME) o;
        if (Math.abs(x - comp.x) > permittedError) return false;
        if (Math.abs(y - comp.y) > permittedError) return false;
        if (Math.abs(z - comp.z) > permittedError) return false;
        return true;
    }
    
    /**
     * are these two vectors the same? they are is they both have the same x,y,
     * and z values.
     *
     * @param o
     *            the object to compare for equality
     * @return true if they are equal
     */
    @Override
    public boolean equals(Object o) {
        return equals(o,0);
    }

    /**
     * <code>hashCode</code> returns a unique code for this vector object based
     * on it's values. If two vectors are logically equivalent, they will return
     * the same hash code value.
     * @return the hash code value of this vector.
     */
    @Override
    public int hashCode() {
        int hash = 37;
        hash += 37 * hash + Double.doubleToLongBits(x);
        hash += 37 * hash + Double.doubleToLongBits(y);
        hash += 37 * hash + Double.doubleToLongBits(z);
        return hash;
    }

    /**
     * <code>toString</code> returns the string representation of this vector.
     * The format is:
     *
     * org.jme.math.Vector3dJME [X=XX.XXXX, Y=YY.YYYY, Z=ZZ.ZZZZ]
     *
     * @return the string representation of this vector.
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

     /**
     * Prints in a way that is friendly for import to excel
     */
    public String toString_DataSet() {
        return x + ", " + y + ", " + z;
    }
    


    public double getX() {
        return x;
    }

    public Vector3dJME setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return y;
    }

    public Vector3dJME setY(double y) {
        this.y = y;
        return this;
    }

    public double getZ() {
        return z;
    }

    public Vector3dJME setZ(double z) {
        this.z = z;
        return this;
    }
    
    /**
     * @param index
     * @return x value if index == 0, y value if index == 1 or z value if index ==
     *         2
     * @throws IllegalArgumentException
     *             if index is not one of 0, 1, 2.
     */
    public double get(int index) {
        switch (index) {
            case 0:
                return x;
            case 1:
                return y;
            case 2:
                return z;
        }
        throw new IllegalArgumentException("index must be either 0, 1 or 2");
    }
    
    /**
     * @param index
     *            which field index in this vector to set.
     * @param value
     *            to set to one of x, y or z.
     * @throws IllegalArgumentException
     *             if index is not one of 0, 1, 2.
     */
    public void set(int index, double value) {
        switch (index) {
            case 0:
                x = value;
                return;
            case 1:
                y = value;
                return;
            case 2:
                z = value;
                return;
        }
        throw new IllegalArgumentException("index must be either 0, 1 or 2");
    }
    public boolean isNaN(){
        if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z)){
            return true;
        }else{
            return false;
        }
    }
    
    public static Vector3dJME ZERO(){
        return new Vector3dJME(0,0,0);
    }

    public boolean isZero() {
        return this.equals(Vector3dJME.ZERO);
    }

    
    public boolean isZero(double accuracy) {
        return (this.lengthSquared()<accuracy*accuracy);
        
    }
    
    public Vector3dJME rotateAsVector(Vector3dJME axis, double angle){
        //see secondarysRotation_axis_inPrimary
        double c=Math.cos(angle);
        double s=Math.sin(angle);
        double t=1-Math.cos(angle);
        
        double newX=(t*axis.x*axis.x+c)*x +
                    (t*axis.x*axis.y+s*axis.z)*y+
                    (t*axis.x*axis.z-s*axis.y)*z;
        
        double newY=(t*axis.x*axis.y-s*axis.z)*x +
                    (t*axis.y*axis.y+c)*y+
                    (t*axis.y*axis.z+s*axis.x)*z;
        
        double newZ=(t*axis.x*axis.z+s*axis.y)*x +
                    (t*axis.y*axis.z-s*axis.x)*y+
                    (t*axis.z*axis.z+c)*z;
        
        return new Vector3dJME(newX,newY,newZ);
    }
    
    public Vector3dJME rotateAsVector(QuaternionD rotationQuaternion){
        Vector3dJME axis=new Vector3dJME();
        double angle=rotationQuaternion.toAngleAxis(axis);
        return this.rotateAsVector(axis,angle);
    }
    public Vector3dJME rotateReverseAsVector(QuaternionD rotationQuaternion){
        Vector3dJME axis=new Vector3dJME();
        double angle=rotationQuaternion.toAngleAxis(axis);
        return this.rotateAsVector(axis,-angle);
    }
    
    
    public int getNumberOfNonZeroAxes(){
        return (x!=0?1:0)+(y!=0?1:0)+(z!=0?1:0);
    }


    
}
