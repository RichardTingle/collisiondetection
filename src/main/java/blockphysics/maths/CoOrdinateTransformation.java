/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;


/**
 *
 * @author Richard
 */
public class CoOrdinateTransformation {
    private final Matrix3 transformation;
    private Matrix3 inverseCashe;
    
    public CoOrdinateTransformation(Vector3dJME localXAxis, Vector3dJME localYAxis,Vector3dJME localZAxis){
        //check that this is a valid orthoganal set of axes 
        //(so dot product is 0, but we'll allow some slack)
        
        if (Math.abs(localXAxis.dot(localYAxis))>0.05 || Math.abs(localXAxis.dot(localZAxis))>0.05  || Math.abs(localYAxis.dot(localZAxis))>0.05){
            throw new RuntimeException("Invalid coOrdinate basis: Not perpendicular");
        }
        if (localXAxis.lengthSquared()<0.99 || localYAxis.lengthSquared()<0.99 || localZAxis.lengthSquared()<0.99){
            String errorText="Invalid coOrdinate basis: vectors are not unit vectors, vector lengths are: ";
            errorText=errorText + localXAxis.length() + ", " + localYAxis.length() + ", " + localZAxis.length();
            throw new RuntimeException(errorText);
        }  
        
        double[][] data=new double[3][3];
        
        data[0][0]=localXAxis.x;
        data[0][1]=localXAxis.y;
        data[0][2]=localXAxis.z;
        
        data[1][0]=localYAxis.x;
        data[1][1]=localYAxis.y;
        data[1][2]=localYAxis.z;
        
        data[2][0]=localZAxis.x;
        data[2][1]=localZAxis.y;
        data[2][2]=localZAxis.z;
        try {
            transformation=new Matrix3(data);
        } catch (InvalidMatrixSizeException ex) {
            //should never happen
            throw new RuntimeException("Invalid coOrdinate basis");
        }
        
    }
    
    public Matrix3 getUnderlyingMatrix(){
        return transformation;
    }
    
    public CoOrdinateTransformation(Matrix3 transformation){
        this.transformation=transformation;
        
    }
    
    private Matrix3 getInverseCashe(){
        if (inverseCashe==null){
            inverseCashe=transformation.getTranspose();
        }
        return inverseCashe;
    }
    
    public CoOrdinateTransformation getInverse(){
       
        return new CoOrdinateTransformation(getInverseCashe());
    }
    
    public Vector3dJME transform(Vector3dJME in){
        return transformation.multiply(in);
    }
    
    public Line transform(Line in){
        Vector3dJME newStart=transform(in.start);
        Vector3dJME newEnd=transform(in.end);
        //centre is calculated on demand
        
        return new Line(newStart,newEnd);

    }
    
    public Matrix3 transform(Matrix3 in){
    
            //formula is BMB(transpose)
            //see pg 407 of Game physics engine development
            return transformation.multiply(in).multiply(getInverseCashe());
    }
    
    @Override
    public String toString(){
        return transformation.toString();
    }
    

    
    
}
