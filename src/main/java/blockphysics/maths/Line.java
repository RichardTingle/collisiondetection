/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.maths;



/**
 *
 * @author Richs
 */
public class Line {
    public final Vector3dJME start;
    public final Vector3dJME end;
    private Vector3dJME centre;
    private Vector3dJME startToEndVector;
    private Vector3dJME startToEndVector_unit;
    private Double length; //Double so can be null
    
    public Line(Vector3dJME start, Vector3dJME end) {
        this.start = start;
        this.end = end;
    }
    
    @Override
    public String toString(){
        return start.toString() + " to " + end.toString();

    }
    
    public Vector3dJME getCentrePoint(){
        if (centre==null){
            centre=new Vector3dJME(0.5*(start.x+end.x),0.5*(start.y+end.y),0.5*(start.z+end.z));
        }
        return centre;
    }
    
    public Vector3dJME getStartToEndVector(){
        if (startToEndVector==null){
            startToEndVector=end.subtract(start);
        }
        return startToEndVector;
    }
    public Vector3dJME getStartToEndVector_unit(){
        if (startToEndVector_unit==null){
            startToEndVector_unit=getStartToEndVector().normalize();
        }
        return startToEndVector_unit;
    }
    public double getLength(){
        if (length==null){
            length=getStartToEndVector().length();
        }
        return length;
    }
    
    public Vector3dJME[] findPointsOfClosestApproach(Line otherLine){
        //see http://geomalgorithms.com/a07-_distance.html Distance between Lines section
        
        //this returns the points on both lines at which the lines are at their
        //closest. If the lines are parallel it returns the mid points of 
        //the two lines (not ideal behaviour but probably fine)
        
        Vector3dJME line1Origin=start;
        Vector3dJME line1Vector=getStartToEndVector();
        
        Vector3dJME line2Origin=otherLine.start;
        Vector3dJME line2Vector=otherLine.getStartToEndVector();
        
        Vector3dJME w0=line1Origin.subtract(line2Origin);
        
        double a=line1Vector.dot(line1Vector);
        double b=line1Vector.dot(line2Vector);
        double c=line2Vector.dot(line2Vector);
        double d=line1Vector.dot(w0);
        double e=line2Vector.dot(w0);
        
        if (a*c-b*b==0){
            //parallel lines, o noes!
            //for the time could return the mid points
            //of the lines (although this is wrong):
            return new Vector3dJME[]{this.getCentrePoint(),otherLine.getCentrePoint()};

            //throw new RuntimeException("Parallel lines are not yet properly handled");
            
            
        }else{
            double proportionAlongLine1=(b*e-c*d)/(a*c-b*b);
            double proportionAlongLine2=(a*e-b*d)/(a*c-b*b); 
            
            Vector3dJME line1Point=this.getPointAProportionAlongLine(proportionAlongLine1);
            Vector3dJME line2Point=otherLine.getPointAProportionAlongLine(proportionAlongLine2);
            
            return new Vector3dJME[]{line1Point,line2Point};
 
        }
    }
    
    public Vector3dJME findCentreOfClosedApproach(Line otherLine){
        Vector3dJME[] closestPoints=findPointsOfClosestApproach(otherLine);
        return closestPoints[0].add(closestPoints[1]).mult(0.5);
    }
    
    public double findDistanceSquaredOfClosestApproach(Vector3dJME otherPoint){
        Vector3dJME closedPoint=findPointOfClosedApproach(otherPoint);
        return closedPoint.distanceSquared(otherPoint);
    }
    
    public Vector3dJME findPointOfClosedApproach(Vector3dJME otherPoint){
       
        double projection=getProjection(otherPoint);
        
        if (projection<0){
            return start;
        }else if(projection>getLength()){
            return end;
        }else{
            return start.add(getStartToEndVector_unit().mult(projection));
        }
    }
    
    private Vector3dJME getPointAProportionAlongLine(double proportion){
        if (proportion<0){
            return new Vector3dJME(start);
        }else if (proportion>1){
            return new Vector3dJME(end);
        }else{
            return start.add(getStartToEndVector().mult(proportion));
        }
    }

    public double getProjection(Vector3dJME otherPoint){
        Vector3dJME pointLocalFrame = otherPoint.subtract(start);
        Vector3dJME lineDirection=getStartToEndVector_unit();
        
        return pointLocalFrame.dot(lineDirection);
    }

    public Vector3dJME getPointADistanceAlongLine(double lengthAlongLine, boolean allowToExtendBeyondLine) {
        if (!allowToExtendBeyondLine && lengthAlongLine<0){
            return start;
        }else if(!allowToExtendBeyondLine && lengthAlongLine>getLength()){
            return end;
        }else{
            return start.add(getStartToEndVector_unit().mult(lengthAlongLine));
        }
    }

    /**
     * this is purely a measure of alignment, the lines physical position is irrelenant to this
     * @param vector 
     */
    public double getProjectionOfVector(Vector3dJME vector) {
        return this.getStartToEndVector_unit().dot(vector);
    }
            
     

    
}
