/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.midphase;


import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The midphase effectively creates an axis aligned bounding box about the object
 * (given its current rotation) and uses that to determine if an object is 
 * either:
 * - Definately not in collision (with the other potential collider passed as 
 * part of the PotentialCollision
 * - Possibly colliding (but not neccissarily)
 * @author Richs
 * @param <T> 
 */
public class MidPhase<T extends MidPhaseCollidable> {
    
    private final ExecutorService threadPool;
    private final int suggestedNumberOfTasksToSplitTo;
    
    /**
     * A Class to filter potential collisions into MAY collide and definately
     * not colliding
     * 
     * @param threadPool A thread pool to use to seperate out the potential 
     * collisions into batches
     * @param seperateBatchesToUse the number of batches to use when processing
     * the potential collisions
     */
    public MidPhase(ExecutorService threadPool, int seperateBatchesToUse){
        this.threadPool=threadPool;
        this.suggestedNumberOfTasksToSplitTo=seperateBatchesToUse;
    }


    
    public Set<PotentialCollision<T>> midPhaseFilterCollisions(Collection<PotentialCollision<T>> input){

        
        
        Set<PotentialCollision<T>> overallData =new HashSet<>();
        
        Set<Future<Set<PotentialCollision<T>>>> futures=new HashSet<>();
        

        int entriesPerInput=input.size()/suggestedNumberOfTasksToSplitTo;
        
        
        Iterator<PotentialCollision<T>> it=input.iterator();
        
        for(int set=0;set<suggestedNumberOfTasksToSplitTo;set++){
            int usedEntries=0;
            Collection<PotentialCollision<T>> thisThreadPotentialCollisions=new ArrayList<>(entriesPerInput);
            while(it.hasNext() && (set==suggestedNumberOfTasksToSplitTo-1 || usedEntries<entriesPerInput)){ //set==numberOfSplits-1  means that the last split takes any odd values that don't exactly split amoungst the others
                usedEntries++;
                thisThreadPotentialCollisions.add(it.next());
            }
            futures.add(threadPool.submit( () -> runMidPhasePiece(thisThreadPotentialCollisions)));
        }
        
        for(Future<Set<PotentialCollision<T>>> future:futures){
            try {
                overallData.addAll(future.get());
            } catch (InterruptedException | ExecutionException ex) {
                throw new RuntimeException("midphase failed during multithreading",ex);
            }
        }
        
        return overallData;
    }

    /**
     * This is a method intended (but not required) to have pieces of the total 
     * mid phase set passed to it for processing each as part of a callable in a 
     * thread pool.
     * 
     * Returns the PotentialCollisions that (by the mid phase rules) MAY collide
     * @param input
     * @return 
     */
    private Set<PotentialCollision<T>> runMidPhasePiece(Collection<PotentialCollision<T>> input){
        //midphase2 is the first phase to consider rotation. But its still working
        //with overall boxes
        
        
        Set<PotentialCollision<T>> passesMidPhase2=new HashSet<>();
        
        Iterator<PotentialCollision<T>> iterator=input.iterator();
        
        Vector3dJME distances_reusable=new Vector3dJME();
        
        while (iterator.hasNext()){
            PotentialCollision<T> potentialCollision=iterator.next();
            
            T collider1=potentialCollision.getCollider1();
            T collider2=potentialCollision.getCollider2();
            
            Vector3dJME collider1Centre=collider1.getCollisionalCentrePosition();
            Vector3dJME collider2Centre=collider2.getCollisionalCentrePosition();
            

            
            
            distances_reusable.x=Math.abs(collider1Centre.x-collider2Centre.x);
            distances_reusable.y=Math.abs(collider1Centre.y-collider2Centre.y);
            distances_reusable.z=Math.abs(collider1Centre.z-collider2Centre.z);
            
            Vector3dJME collider1MaxExtents=collider1.getMaxRadiusForAxes();
            Vector3dJME collider2MaxExtents=collider2.getMaxRadiusForAxes();
            
            Vector3dJME collisionExtents=new Vector3dJME();
            collisionExtents.x=collider1MaxExtents.x+collider2MaxExtents.x;
            collisionExtents.y=collider1MaxExtents.y+collider2MaxExtents.y;
            collisionExtents.z=collider1MaxExtents.z+collider2MaxExtents.z;
            

            //for each co-ordinate test the distance
            if (distances_reusable.x>collisionExtents.x || distances_reusable.y>collisionExtents.y || distances_reusable.z>collisionExtents.z){
                //cannot collide
            }else{
                passesMidPhase2.add(potentialCollision);
            }
        }
        
        return passesMidPhase2;
        
    }
    
}
