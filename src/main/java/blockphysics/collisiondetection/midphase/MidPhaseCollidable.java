/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.midphase;

import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public interface MidPhaseCollidable {
     /**
     * A point considered the centre of the collidable.
     * DOES NOT NEED TO BE (but may be) the Centre of Mass
     * @return 
     */
    Vector3dJME getCollisionalCentrePosition();
    
    /**
     * Returns the half maximum extents (from the CollisionalCentrePosition)
     * along the real world axes that the object is garanteed to be within
     * @return 
     */
    Vector3dJME getMaxRadiusForAxes();
    
}
