/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;


import blockphysics.collisiondetection.ActualCollision;
import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.collisiondetection.collisionshape.Box;
import blockphysics.maths.CoOrdinateTransformation;
import blockphysics.maths.Line;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Richard
 * @param <T>
 */
public class NarrowPhase_BoxLevel<T extends NarrowPhaseCollidable> {


    public Collection<ActualCollision<T>> getCollisionData(PotentialCollision<T> potentialCollision, SeperatingAxisSet seperatingAxisSet,Collection<PotentialBreakDownBoxCollision> potentiaBreakDownBoxlCollisions){
        
        Collection<ActualCollision<T>> actualCollisions=new HashSet<>(); 
        PhysicsReusuableObjects physicsReusableObjects=new PhysicsReusuableObjects();
        
        
        for (PotentialBreakDownBoxCollision potentialBreakDownBoxCollision : potentiaBreakDownBoxlCollisions) {

            Collection<Box> box1References=potentialBreakDownBoxCollision.getBreakDownBox1().getBoxesInThisAndChildren();
            Collection<Box> box2References=potentialBreakDownBoxCollision.getBreakDownBox2().getBoxesInThisAndChildren();

            for(Box boxIn1:box1References){
                for(Box boxIn2:box2References){
                    boolean collides=true; //starts under the assumtion of colliding, tries to disprove

                    Iterator<SeperatingAxis> allAxes= seperatingAxisSet.iterator();
                    
                    while(collides==true && allAxes.hasNext()){
                        SeperatingAxis axis = allAxes.next();
                        
                        GlobalAxisShadow shadow1=boxIn1.getBreakdownBoxShadow(axis.getAxisInObjectFrame(potentialCollision.getCollider1()), physicsReusableObjects.reusableShadow1, physicsReusableObjects);
                        GlobalAxisShadow shadow2=boxIn2.getBreakdownBoxShadow(axis.getAxisInObjectFrame(potentialCollision.getCollider2()), physicsReusableObjects.reusableShadow2, physicsReusableObjects);
                        collides=shadow1.overlapsWith(shadow2);
                    }

                    if (collides==true){
                        ActualCollision actualCollision=createCollisionData(potentialCollision,seperatingAxisSet,boxIn1,boxIn2);
              
                        actualCollisions.add(actualCollision);

                    }
                }
            }

        }
        
        return actualCollisions;
    }

    /**
     * Takes a potentialCollision that has already been determined to occure and 
     * does a deeper analysis of it; filling in any collision data etc 
     * @param potentialCollision
     * @param seperatingAxes
     * @param box1Reference
     * @param box2Reference
     * @return 
     */
    private ActualCollision<T> createCollisionData(PotentialCollision<T> potentialCollision,SeperatingAxisSet seperatingAxes,Box box1Reference,Box box2Reference){
        //box1Reference and box2Reference indicates which of the boxes in the box based collision share are/could be colliding
        
        T object1=potentialCollision.getCollider1();
        T object2=potentialCollision.getCollider2();
        PhysicsReusuableObjects physicsReusableObjects=new PhysicsReusuableObjects();

        SeperatingAxis bestAxis=null;
        double minimumDepth=Double.POSITIVE_INFINITY;
        double signedDepth=Double.POSITIVE_INFINITY; //useful for constructing the collision vector
        
        for(SeperatingAxis seperatingAxis: seperatingAxes){
            GlobalAxisShadow shadow1=box1Reference.getBreakdownBoxShadow(seperatingAxis.getAxisInObjectFrame(potentialCollision.getCollider1()), physicsReusableObjects.reusableShadow1, physicsReusableObjects);
            GlobalAxisShadow shadow2=box2Reference.getBreakdownBoxShadow(seperatingAxis.getAxisInObjectFrame(potentialCollision.getCollider2()), physicsReusableObjects.reusableShadow2, physicsReusableObjects);
            AxisShadowOverlap overlapShadow=shadow1.getOverlapShadow(shadow2);
            
            double depth=overlapShadow.getShadowLength();
            if (overlapShadow.formedFromCompletelyEncloses() && depth<minimumDepth){  //formedFromCompletelyEncloses is a check that indicated if one object completely contains the other on the axis, such axes are not valid for mimimum penitration depth
                minimumDepth=depth;
                bestAxis=seperatingAxis;
                signedDepth=overlapShadow.getSignedShadowLength(); //useful for collision vector, goes from object 2 to 1 (object 1's perspective)
            }
        }



        //the collision vector is the minimum depth along
        //the axis along which that occurs going from object
        //0 to object 1

        Vector3dJME collisionVector;
        Vector3dJME collisionPoint;
        
        if (bestAxis==null){
            //if the best axis is null then the the collision shapes is fully enclosed
            //this is clearly bad, we substitude in smaller boxes corner to corner distance
            //(i.e. its largest length) in the centre to centre direction, the collision 
            //will probably not be fully resolved on this cycle but we can cope this that
            
            Box enclodingBox1=potentialCollision.getCollider1().getEnclosingBox();
            Box enclodingBox2=potentialCollision.getCollider2().getEnclosingBox();
            

            double cornerToCorner1=enclodingBox1.getCornerToCornerDistance();
            double cornerToCorner2=enclodingBox2.getCornerToCornerDistance();

            double smallerCornerToCorner=(cornerToCorner1<cornerToCorner2?cornerToCorner1:cornerToCorner2);

            collisionVector=object2.getCOMPosition().subtract(object1.getCOMPosition()).normalize().mult(smallerCornerToCorner);
            collisionPoint=object1.getCOMPosition().add(object2.getCOMPosition()).mult(0.5);
        }else{
            //standard and desired behaviour
            
            
            //if the two boxes precisely match axes (tends to happen at the very 
            //beginning of a simulation where everything is flat) then the best
            //axis can be effectively chosen randomly between them, this is very 
            //very bad. Imagine a very small box falling onto a very large box, if
            //the large box's axis is chosen as the best axis then the small
            //boxes corners are considered to determine penitration, all good. But 
            //if the smaller box's axis is used then then the largest box's coners 
            //are used and they might be miles from the actual collision

            if (bestAxis.getAxisType()!=AxisType.CROSSPRODUCTAXIS){
                //could be degenerate, must make sure it isn't
                for(SeperatingAxis seperatingAxis: seperatingAxes){
                    if (bestAxis.isDegenerateWith(seperatingAxis)){
                        //degenerate axis, ensure we have the one associated with the
                        //smallest face

                        if (seperatingAxis.getAxisType()==AxisType.CROSSPRODUCTAXIS){
                            //I don't really know what to do in this case so i'm going
                            //to go with nothing
                            continue;
                        }else{
                            //we want the bestAxis to come from the face with the 
                            //greatest area (as per comment above)

                            double currentBestAxisFaceArea;
                            double alternativeAxisFaceArea;

                            T bestAxisObject;
                            T alternativeAxisObject;

                            Box bestAxisReference;
                            Box alternativeAxisReference;

                            AxisType bestAxisType=bestAxis.getAxisType();
                            AxisType alternativeAxisType=seperatingAxis.getAxisType();

                            if (bestAxisType==AxisType.COLLIDER1FACEAXIS){
                                bestAxisObject=object1;
                                bestAxisReference=box1Reference;
                            }else{
                                bestAxisObject=object2;
                                bestAxisReference=box2Reference;
                            }

                            if (alternativeAxisType==AxisType.COLLIDER1FACEAXIS){
                                alternativeAxisObject=object1;
                                alternativeAxisReference=box1Reference;
                            }else{
                                alternativeAxisObject=object2;
                                alternativeAxisReference=box2Reference;
                            }


                            ArrayList<SeperatingAxis> bestAxisOtherAxes=new ArrayList<>(seperatingAxes.getSeperatingAxesOfType(bestAxisType));
                            bestAxisOtherAxes.remove(bestAxis);

                            ArrayList<GlobalAxisShadow> bestAxisShadows=getAxisShadows(bestAxisOtherAxes,bestAxisObject,bestAxisReference);

                            ArrayList<SeperatingAxis> alternativeAxisOtherAxes=new ArrayList<>(seperatingAxes.getSeperatingAxesOfType(alternativeAxisType));
                            alternativeAxisOtherAxes.remove(seperatingAxis);

                            ArrayList<GlobalAxisShadow> alternativeAxisShadows=getAxisShadows(alternativeAxisOtherAxes,alternativeAxisObject,alternativeAxisReference);

                            currentBestAxisFaceArea=bestAxisShadows.get(0).getLength()*bestAxisShadows.get(1).getLength();
                            alternativeAxisFaceArea=alternativeAxisShadows.get(0).getLength()*alternativeAxisShadows.get(1).getLength();

                            if (alternativeAxisFaceArea>currentBestAxisFaceArea){
                                //we want the best axis to be from the face with the largest surface area
                                bestAxis=seperatingAxis;
                            }
                        }

                    }
                }

            }
            
            
            collisionVector=(bestAxis.getAxis().mult(signedDepth));
            
            switch (bestAxis.getAxisType()) {
                case COLLIDER1FACEAXIS:{
                    ArrayList<? extends SeperatingAxis> edgeObjectFaceAxes=seperatingAxes.getSeperatingAxesOfType(AxisType.COLLIDER2FACEAXIS);
                    ArrayList<GlobalAxisShadow> shadows=getAxisShadows(edgeObjectFaceAxes,object2,box2Reference);
                    collisionPoint=getCollisionPointVertexVsFace(bestAxis,signedDepth,edgeObjectFaceAxes,shadows);
                        break;
                    }
                case COLLIDER2FACEAXIS:{
                    ArrayList<? extends SeperatingAxis> edgeObjectFaceAxes=seperatingAxes.getSeperatingAxesOfType(AxisType.COLLIDER1FACEAXIS);
                    ArrayList<GlobalAxisShadow> shadows=getAxisShadows(edgeObjectFaceAxes,object1,box1Reference);
                    collisionPoint=getCollisionPointVertexVsFace(bestAxis,-signedDepth,edgeObjectFaceAxes,shadows);
                        break;
                    }
                default:
                    //cross product based axis
                    
                    ArrayList<? extends SeperatingAxis> object1FaceAxes=seperatingAxes.getSeperatingAxesOfType(AxisType.COLLIDER1FACEAXIS);
                    ArrayList<GlobalAxisShadow> object1Shadows=getAxisShadows(object1FaceAxes,object1,box1Reference);
                    ArrayList<? extends SeperatingAxis> object2FaceAxes=seperatingAxes.getSeperatingAxesOfType(AxisType.COLLIDER2FACEAXIS);
                    ArrayList<GlobalAxisShadow> object2Shadows=getAxisShadows(object2FaceAxes,object2,box2Reference);
                    collisionPoint=getCollisionPointEdgeVsEdge((SeperatingAxisFromCrossProduct)bestAxis, object1FaceAxes, object1Shadows,object2FaceAxes, object2Shadows);
                    break;
            }
            
            
        }

        return new ActualCollision<>(potentialCollision,collisionPoint,collisionVector);
    }
    
    
   
    
    private static Vector3dJME getCollisionPointVertexVsFace(SeperatingAxis axis, double signedDepth, ArrayList<? extends SeperatingAxis> edgeObjectFaceAxes, ArrayList<GlobalAxisShadow> edgeObjectShadows ){
        //the normal of the face is along the axis but points towards the other object
        
        //signedDepth allows us to tell which direction the face is pointing, 
        //signedDepth goes from object 1 to 0 (object 0's perspective) along the 
        //seperating axis, so if its +ve
        
        Vector3dJME faceNormal=(signedDepth>0?axis.getAxis():axis.getAxis().negate());
        
        //now we need to determine which of the 8 corners is the vertex that intersected the face
        //for each axis we want the point on the line (represented by the mins and maxs on the
        //other 3 Seperating axes) that are at the minimum amount on the main seperating axis
        
        if (edgeObjectFaceAxes.size()!=3){
            throw new RuntimeException("3 edgeObjectFaceAxes expected (as a box has 3 face normals (+- are not counted)");
        }
        
        CoOrdinateTransformation fromWorldToEdgeObject;
        try{
             fromWorldToEdgeObject=new CoOrdinateTransformation(edgeObjectFaceAxes.get(0).getAxis(),edgeObjectFaceAxes.get(1).getAxis(),edgeObjectFaceAxes.get(2).getAxis());
        }catch(RuntimeException e){
            throw new RuntimeException("edgeObjectFaceAxes were invalid, they should form a coOrdinate system. ",e);
        }
        
        Vector3dJME collisionPointLocalFrame=new Vector3dJME();
        
        //the question determines if we want high or low values on this axis
        collisionPointLocalFrame.x=(faceNormal.dot(edgeObjectFaceAxes.get(0).getAxis())>0?edgeObjectShadows.get(0).getStart(): edgeObjectShadows.get(0).getEnd());
        collisionPointLocalFrame.y=(faceNormal.dot(edgeObjectFaceAxes.get(1).getAxis())>0?edgeObjectShadows.get(1).getStart(): edgeObjectShadows.get(1).getEnd());
        collisionPointLocalFrame.z=(faceNormal.dot(edgeObjectFaceAxes.get(2).getAxis())>0?edgeObjectShadows.get(2).getStart(): edgeObjectShadows.get(2).getEnd());
        
        //convert to global
        return fromWorldToEdgeObject.getInverse().transform(collisionPointLocalFrame);
        

        
    }

    private static Vector3dJME getCollisionPointEdgeVsEdge(SeperatingAxisFromCrossProduct axis, ArrayList<? extends SeperatingAxis> object1FaceAxes, ArrayList<GlobalAxisShadow> object1Shadows,ArrayList<? extends SeperatingAxis> object2FaceAxes, ArrayList<GlobalAxisShadow> object2Shadows ){
        Vector3dJME obj1OriginatorAxis=axis.getOriginatorAxis1();
        Vector3dJME obj2OriginatorAxis=axis.getOriginatorAxis2();

        //each of the originator axes are associated with 4 edges, we want to
        //determine which edge was actually involed in the collision. This
        //will be the edge whose centre point is closest to the other objects centre
        
        CoOrdinateTransformation fromWorldToObject1;
        try{
             fromWorldToObject1=new CoOrdinateTransformation(object1FaceAxes.get(0).getAxis(), object1FaceAxes.get(1).getAxis(), object1FaceAxes.get(2).getAxis());
        }catch(RuntimeException e){
            throw new RuntimeException("edgeObjectFaceAxes were invalid, they should form a coOrdinate system. " +e.getMessage());
        }
        
        CoOrdinateTransformation fromWorldToObject2;
        try{
             fromWorldToObject2=new CoOrdinateTransformation(object2FaceAxes.get(0).getAxis(), object2FaceAxes.get(1).getAxis(), object2FaceAxes.get(2).getAxis());
        }catch(RuntimeException e){
            throw new RuntimeException("edgeObjectFaceAxes were invalid, they should form a coOrdinate system. " +e.getMessage());
        }
        

        
        Vector3dJME box1Centre=getBoxCentre(fromWorldToObject1,object1Shadows);
        Vector3dJME box2Centre=getBoxCentre(fromWorldToObject2,object2Shadows);

        Line object1Edge=getCollisionEdge2(fromWorldToObject1,box2Centre,obj1OriginatorAxis,object1FaceAxes,object1Shadows);
        Line object2Edge=getCollisionEdge2(fromWorldToObject2,box1Centre,obj2OriginatorAxis,object2FaceAxes,object2Shadows);
        
        
        return object1Edge.findCentreOfClosedApproach(object2Edge);
        
    }

    private static Line getCollisionEdge2(CoOrdinateTransformation fromWorldToObject,Vector3dJME otherBoxCentre,Vector3dJME originatorAxis,ArrayList<? extends SeperatingAxis> objectFaceAxes, ArrayList<GlobalAxisShadow> objectShadows){
        //the shadows on the two axes that are not equal to the originator axes 
        //determine the 4 edges, the originator axis determines the start/end of 
        //the edge. But the shadows are defined in the seperating axis frame, we convert
        //to world frame
        
        boolean axis0IsOriginator;
        boolean axis1IsOriginator;
        
        //the arrayLost.get() method is remarkably a major contributor to this methods time
        //and this method is called a huge number of times so;
        SeperatingAxis localX=objectFaceAxes.get(0);
        SeperatingAxis localY=objectFaceAxes.get(1);
        SeperatingAxis localZ=objectFaceAxes.get(2);
        
        GlobalAxisShadow localXShadow=objectShadows.get(0);
        GlobalAxisShadow localYShadow=objectShadows.get(1);
        GlobalAxisShadow localZShadow=objectShadows.get(2);
        
        if (localX.getAxis().equals(originatorAxis)){
            axis0IsOriginator=true;
            axis1IsOriginator=false;

        }else if (localY.getAxis().equals(originatorAxis)){
            axis0IsOriginator=false;
            axis1IsOriginator=true;
        }else{
            axis0IsOriginator=false;
            axis1IsOriginator=false;
        }
        
        Vector3dJME otherBoxCentreLocalCoOrds=fromWorldToObject.transform(otherBoxCentre);
        
        Line currentBestEdgeLocalCoOrds=null;
        double currentEdgeDistanceSquared=Double.POSITIVE_INFINITY;
        
        for(int axis1StartEnd=0;axis1StartEnd<2;axis1StartEnd++){
            for(int axis2StartEnd=0;axis2StartEnd<2;axis2StartEnd++){
                Vector3dJME lineStart;
                Vector3dJME lineEnd;
                if (axis0IsOriginator){
                    lineStart=new Vector3dJME(localXShadow.getStart(),localYShadow.getMinimumOrMaximum(axis1StartEnd),localZShadow.getMinimumOrMaximum(axis2StartEnd));
                    lineEnd=new Vector3dJME(localXShadow.getEnd(),localYShadow.getMinimumOrMaximum(axis1StartEnd),localZShadow.getMinimumOrMaximum(axis2StartEnd));
                
                }else if (axis1IsOriginator){
                    lineStart=new Vector3dJME(localXShadow.getMinimumOrMaximum(axis1StartEnd),localYShadow.getStart(),localZShadow.getMinimumOrMaximum(axis2StartEnd));
                    lineEnd=new Vector3dJME(localXShadow.getMinimumOrMaximum(axis1StartEnd),localYShadow.getEnd(),localZShadow.getMinimumOrMaximum(axis2StartEnd));
                }else{
                    lineStart=new Vector3dJME(localXShadow.getMinimumOrMaximum(axis1StartEnd),localYShadow.getMinimumOrMaximum(axis2StartEnd),localZShadow.getStart());
                    lineEnd=new Vector3dJME(localXShadow.getMinimumOrMaximum(axis1StartEnd),localYShadow.getMinimumOrMaximum(axis2StartEnd),localZShadow.getEnd());                    
                }
                
                Line edgeLine=new Line(lineStart,lineEnd);
                double distanceSquared=otherBoxCentreLocalCoOrds.distanceSquared(edgeLine.getCentrePoint());
                
                if (distanceSquared<currentEdgeDistanceSquared){
                     currentBestEdgeLocalCoOrds=edgeLine;
                     currentEdgeDistanceSquared=distanceSquared;
                }
            }
        }
        
        return fromWorldToObject.getInverse().transform(currentBestEdgeLocalCoOrds);
    }
    
    
    private static Vector3dJME getBoxCentre(CoOrdinateTransformation fromWorldToObject, ArrayList<GlobalAxisShadow> object1Shadows){
        return fromWorldToObject.getInverse().transform(
                new Vector3dJME(0.5*object1Shadows.get(0).getStart()+object1Shadows.get(0).getEnd(),
                                0.5*object1Shadows.get(1).getStart()+object1Shadows.get(1).getEnd(),
                                0.5*object1Shadows.get(2).getStart()+object1Shadows.get(2).getEnd()
                )
           );
    }
    
    private ArrayList<GlobalAxisShadow> getAxisShadows(ArrayList<? extends SeperatingAxis> seperatingAxes, T object, Box boxReference){

        ArrayList<GlobalAxisShadow> shadows=new ArrayList<>(seperatingAxes.size());
        for(SeperatingAxis axis : seperatingAxes){
            GlobalAxisShadow shadow = boxReference.getBreakdownBoxShadow(axis.getAxisInObjectFrame(object), new GlobalAxisShadow(), new PhysicsReusuableObjects());
            shadows.add(shadow);

        }
        return shadows;
    }




}
