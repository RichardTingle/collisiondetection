/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;


import blockphysics.collisiondetection.ActualCollision;
import blockphysics.collisiondetection.PotentialCollision;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class will filter collidables between DEFINATELY colliding and DEFINATELY
 * not colliding. It is the most expensive per assessment so earlier phases 
 * should have filtered down the number of potential collisions
 * @author Richard
 * @param <T>
 */
public class NarrowPhase<T extends NarrowPhaseCollidable> {

    private ExecutorService threadPool;
    private int seperateBatchesToUse;
    
    private NarrowPhase_BreakDownBox<T> narrowPhase_BreakDownBox = new NarrowPhase_BreakDownBox<>();
    private NarrowPhase_BoxLevel<T> narrowPhase_boxLevel = new NarrowPhase_BoxLevel<>();
    
    public NarrowPhase(ExecutorService threadPool, int seperateBatchesToUse){
        this.threadPool=threadPool;
        this.seperateBatchesToUse=seperateBatchesToUse;
    }
    
    
    public Set<ActualCollision<T>> runNarrowPhase_multithreaded(Set<PotentialCollision<T>> input){

        
        Set<ActualCollision<T>> overallData =new HashSet<>();
        
        Set<Future<Set<ActualCollision<T>>>> futures=new HashSet<>();
        
        int numberOfSplits=seperateBatchesToUse;
        int entriesPerInput=input.size()/numberOfSplits;
        
        Iterator<PotentialCollision<T>> it=input.iterator();
        
        for(int set=0;set<numberOfSplits;set++){
            int usedEntries=0;
            Collection<PotentialCollision<T>> thisThreadPotentialCollisions=new ArrayList<>(entriesPerInput);
            while(it.hasNext() && (set==numberOfSplits-1 || usedEntries<entriesPerInput)){ //set==numberOfSplits-1  means that the last split takes any odd values that don't exactly split amoungst the others
                usedEntries++;
                thisThreadPotentialCollisions.add(it.next());
            }
            futures.add(threadPool.submit( ()-> runNarrowPhase(thisThreadPotentialCollisions)));
        }
        
        for(Future<Set<ActualCollision<T>>> future:futures){
            try {
                overallData.addAll(future.get());
            } catch (InterruptedException | ExecutionException ex) {
                throw new RuntimeException("narrow phase failed during multithreading", ex);
            }
        }
        
        return overallData;
    }
    
    private Set<ActualCollision<T>> runNarrowPhase(Collection<PotentialCollision<T>> input){

        
        //uses seperate axis theorem:
        //http://www.metanetsoftware.com/technique/tutorialA.html
        
        //because we're in 3d we need to consider some bonus axes
        //http://gamedev.stackexchange.com/questions/44500/how-many-and-which-axes-to-use-for-3d-obb-collision-with-sat
        //basically the first 6 axis only consider vertex vs face collisions, but 
        //we also need to test edge vs edge collisions, these are found on the 
        //cross products of the initial two sets of 3 axes (a bonus 9 axes)
        
        Set<ActualCollision<T>> output=new HashSet<>();
        
        Iterator<PotentialCollision<T>> iterator=input.iterator();
        
        PhysicsReusuableObjects reusableObjects=new PhysicsReusuableObjects();
        
        while (iterator.hasNext()){

            Collection<ActualCollision<T>> actualCollisions = determineCollision(iterator.next(),reusableObjects);
            output.addAll(actualCollisions);
        }
        
        
        
        
        return output;
        
    }
    
    
    /**
     * Takes a single potential collisions and returns a collection of actual 
     * collisions as filtered by the narrow phase (note the number of real
     * collisions can be larger than the single PotentialCollision because the
     * objects may collide in more than one place
     * @param potentialCollision
     * @param reusableObjects
     * @return 
     */
    private Collection<ActualCollision<T>> determineCollision(PotentialCollision<T> potentialCollision,PhysicsReusuableObjects reusableObjects){
        
        
        T object1=potentialCollision.getCollider1();
        T object2=potentialCollision.getCollider2();
        SeperatingAxisSet seperatingAxisSet=new SeperatingAxisSet(object1,object2);

        ArrayList<PotentialBreakDownBoxCollision> bottomLevelHopper;
        

        
        //NarrowPhase_BreakDownBox concerns itself with doing all the breakdownbox vs breakdown box stuff
        bottomLevelHopper=narrowPhase_BreakDownBox.getBottomLayerPotentialCollisions(seperatingAxisSet, object1, object2,reusableObjects);
        
        Collection<ActualCollision<T>> actualCollisions = narrowPhase_boxLevel.getCollisionData(potentialCollision, seperatingAxisSet, bottomLevelHopper);
        return actualCollisions;
    }
    

    



    

}
