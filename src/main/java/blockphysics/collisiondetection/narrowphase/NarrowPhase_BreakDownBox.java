/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.collisiondetection.collisionshape.BreakDownBox;
import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Richard
 * @param <T>
 */
public class NarrowPhase_BreakDownBox<T extends NarrowPhaseCollidable> {
    //this is a subsystem of the narrow phase, its purpose is to take two objects
    //that potentially collide and generate an arraylist of boxes that could collide
    
    /**
     * This makes breakdownbox vs breakdownbox collisions; breaking down or 
     * rejecting the collisions until it got down to the box vs box layer. Those
     * are returned as box level potential collisions.
     * @param seperatingAxisSet
     * @param object1
     * @param object2
     * @param reusableObjects
     * @return 
     */
    public ArrayList<PotentialBreakDownBoxCollision> getBottomLayerPotentialCollisions(SeperatingAxisSet seperatingAxisSet, T object1, T object2, PhysicsReusuableObjects reusableObjects ){
        
        //this runs as a 'hopper' of tests to perform, each loop takes an item from
        //the hopper and processes it to test for collision, if it collides then it breaks
        //open both boxes and adds 
        
        
        ArrayList<PotentialBreakDownBoxCollision> hopper=new ArrayList<>();
        ArrayList<PotentialBreakDownBoxCollision> bottomLevelHopper=new ArrayList<>(); //for when both breakdown boxes are bottom layer


        //start of the hopper by putting in the two top level breakdownboxes
        hopper.add(new PotentialBreakDownBoxCollision(object1.getTopLevelBreakDownBox(),object2.getTopLevelBreakDownBox()));

        while (hopper.isEmpty()==false){
            //the get the LAST!!! entry in the hopper, this entry must be removed
            //before adding any more because removing the last entry of an array list is cheap
            //all other removes are super slow
            PotentialBreakDownBoxCollision potentialBreakdownCollision=hopper.get(hopper.size()-1);
            hopper.remove(hopper.size()-1);

            boolean couldCollide=true; //starts under the assumtion of colliding, tries to disprove

            Iterator<SeperatingAxis> allAxes=seperatingAxisSet.iterator();
              
            while(couldCollide==true && allAxes.hasNext()){
                SeperatingAxis axis=allAxes.next();
                SeperatingAxisLocalFrame frame1SeperatingAxis=axis.getAxisInObjectFrame(object1);
                SeperatingAxisLocalFrame frame2SeperatingAxis=axis.getAxisInObjectFrame(object2);
                couldCollide=testOverlap(potentialBreakdownCollision.getBreakDownBox1(),potentialBreakdownCollision.getBreakDownBox2(),frame1SeperatingAxis,frame2SeperatingAxis,reusableObjects);
            }
            
            if (couldCollide){ //failed to disprove, break down the box to its substructure

                //we break down the box and see what collisions there are on the lower layer
                if (potentialBreakdownCollision.getBreakDownBox1().isBottomLevelBox() && potentialBreakdownCollision.getBreakDownBox2().isBottomLevelBox()){
                    //they're both at the bottom level, as far as this part of the process is concerned they may collide
                    bottomLevelHopper.add(potentialBreakdownCollision);
                }else{
                    ArrayList<BreakDownBox> box1Substructure=breakDownBox(potentialBreakdownCollision.getBreakDownBox1());
                    ArrayList<BreakDownBox> box2Substructure=breakDownBox(potentialBreakdownCollision.getBreakDownBox2());

                    //add all combinations (between 4 and 9 depending on if the boxes had overlaps)
                    //to the hopper

                    for(int j=0;j<box1Substructure.size();j++){
                        for(int k=0;k<box2Substructure.size();k++){
                            hopper.add(new PotentialBreakDownBoxCollision(box1Substructure.get(j),box2Substructure.get(k)));
                        }
                    }
                } 
            }

        }
        
        return bottomLevelHopper; //contains only the potential breakdown box collisions that contain actual boxes (no substructure)
    }
    
    
    
    private boolean testOverlap(BreakDownBox box1,BreakDownBox box2,SeperatingAxisLocalFrame seperatingAxis1,SeperatingAxisLocalFrame seperatingAxis2 ,PhysicsReusuableObjects reusableObjects){
        //this tests if the two breakdown boxes overlap on the specified axis,
        //NOTE axis 1 and axis 2 are the same axis but in the co-ordinate frame
        //of boxes 1 and 2, the two offsets are are to account for the fact that
        //the calculations assume the axes are at the local 0,0 of the box
        
        GlobalAxisShadow box1Shadow=box1.getBreakdownBoxShadow(seperatingAxis1,reusableObjects.reusableShadow1, reusableObjects);

        
        GlobalAxisShadow box2Shadow=box2.getBreakdownBoxShadow(seperatingAxis2,reusableObjects.reusableShadow2,reusableObjects);

        //so we have the shadows, overlap is simple to discover
        return box1Shadow.overlapsWith(box2Shadow);
        
    }

    
    
    
    
    
    private static ArrayList<BreakDownBox> breakDownBox(BreakDownBox box){
        //takes the inputted box and 'breaks it down' to the lower level (2 or 3 boxes 
        //depending on if it has overlaps) and returns the substructure as an array
        //if the input is bottom layer it just returns the input
        
        ArrayList<BreakDownBox> returnBoxes;
        
        if (box.isBottomLevelBox()){
            returnBoxes=new ArrayList<BreakDownBox>(1);
            returnBoxes.add(box);

        }else{
            returnBoxes=new ArrayList<BreakDownBox>(3);
            returnBoxes.add(box.getSubBox1());
            returnBoxes.add(box.getSubBox2());
            if (box.hasOverlapBox()){
                returnBoxes.add(box.getOverlapBox());
            }
        }
        
        for(int i=0;i<returnBoxes.size();i++){
            if (returnBoxes.get(i)==null){
                String bonusErrorMessage="";
                switch (i){
                        case 0: bonusErrorMessage="SubBox1";
                        case 1: bonusErrorMessage="SubBox2";  
                        case 2: bonusErrorMessage="OverlapBox";
                }
                
                throw new RuntimeException("Breakdown: Null pointer exception-" + bonusErrorMessage);
            }
        }
        
        if (returnBoxes.isEmpty()){
            throw new RuntimeException("Breakdown: Substructure Misssing");
        }
        
        return returnBoxes;
    }

    
}
