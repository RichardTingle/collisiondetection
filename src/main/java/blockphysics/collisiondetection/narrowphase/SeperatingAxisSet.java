/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;


import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * The SeperatingAxisSet is a set of all seperating axes (see seperarting Axes 
 * theorum) for a pair of objects)
 * @author Richs
 */
public class SeperatingAxisSet implements Iterable<SeperatingAxis>{
    

    

    private final Vector3dJME[] axes1;
    private final Vector3dJME[] axes2;
    
    private final ArrayList<SeperatingAxis> col1FaceAxes;
    private final ArrayList<SeperatingAxis> col2FaceAxes;
    private final ArrayList<SeperatingAxisFromCrossProduct> crossProductAxes;
    
    private int noOfcol1FaceAxesInitialised=0;
    private int noOfcol2FaceAxesInitialised=0;
    private boolean crossProductInitialised=false;
    
    public SeperatingAxisSet(NarrowPhaseCollidable collidable1, NarrowPhaseCollidable collidable2){

        
        axes1=collidable1.getFaceAxes();
        axes2=collidable2.getFaceAxes();

        col1FaceAxes=new ArrayList<>(axes1.length);
        
        col2FaceAxes=new ArrayList<>(axes2.length);

        crossProductAxes=new ArrayList<>(axes1.length*axes2.length);
    
        //for efficiency only lazy generates seperating axes

    }

    private void ensureCol1FaceAxesOfIndexCreated(int index){
        if (index<=noOfcol1FaceAxesInitialised){
            for(int i=noOfcol1FaceAxesInitialised;i<=index;i++){
                noOfcol1FaceAxesInitialised++;
                col1FaceAxes.add(new SeperatingAxis(axes1[index],AxisType.COLLIDER1FACEAXIS));
            }

        }
    }
    private void ensureCol2FaceAxesOfIndexCreated(int index){
        if (index<=noOfcol2FaceAxesInitialised){
            for(int i=noOfcol2FaceAxesInitialised;i<=index;i++){
                noOfcol2FaceAxesInitialised++;
                col2FaceAxes.add(new SeperatingAxis(axes2[index],AxisType.COLLIDER2FACEAXIS));
            }

        }
    }
    private void ensureAllCrossProductAxes(){
        if (!crossProductInitialised){
            crossProductInitialised=true;
            for(int i=0;i<axes1.length;i++){
                for(int j=0;j<axes2.length;j++){
                    double axesDotProduct=Math.abs(axes1[i].dot(axes2[j]));
                    if (axesDotProduct>0 && axesDotProduct<1){ //check for degenerate axes
                        crossProductAxes.add(new SeperatingAxisFromCrossProduct(axes1[i],axes2[j]));
                    }

                }
            }
        }
    }
    
    private SeperatingAxis getSeperatingAxisOfIndex(int index){
        if (index<axes1.length){
            ensureCol1FaceAxesOfIndexCreated(index);
            return col1FaceAxes.get(index);
        }else if(index<(axes1.length+axes2.length)){
            index=(index-axes1.length);
            ensureCol2FaceAxesOfIndexCreated(index);
            return col2FaceAxes.get(index);
        }else{
            index=(index-axes1.length-axes2.length);
            ensureAllCrossProductAxes();
            if (index<this.crossProductAxes.size()){
                return crossProductAxes.get(index);
            }else{
                return null;
            }
        }
    }
    
    
    public ArrayList<? extends SeperatingAxis> getSeperatingAxesOfType(AxisType type){
        if (type==AxisType.COLLIDER1FACEAXIS){
            return col1FaceAxes;
        }else if(type==AxisType.COLLIDER2FACEAXIS){
            return col2FaceAxes;
        }else if(type==AxisType.CROSSPRODUCTAXIS){
            return crossProductAxes;
        }else{

            throw new RuntimeException("No such collection");
        }
    }

    
    
    @Override
    public Iterator<SeperatingAxis> iterator() {
        Iterator<SeperatingAxis> it = new Iterator<SeperatingAxis>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return getSeperatingAxisOfIndex(currentIndex)!=null;
            }

            @Override
            public SeperatingAxis next() {
                SeperatingAxis returnAxis=getSeperatingAxisOfIndex(currentIndex);
                currentIndex++;
                return returnAxis;
            }

            @Override
            public void remove() {
               throw new UnsupportedOperationException("Remove operation is not supported");
            }
        };
        return it;
    }
}
