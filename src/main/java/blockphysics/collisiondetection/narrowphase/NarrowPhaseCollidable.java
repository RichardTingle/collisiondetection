/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.collisiondetection.collisionshape.Box;
import blockphysics.collisiondetection.collisionshape.BreakDownBox;
import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public interface NarrowPhaseCollidable {
    BreakDownBox getTopLevelBreakDownBox();
    
    Vector3dJME convertGlobalAxisToLocal(Vector3dJME axis);

    Vector3dJME getCOMPosition();
    
    public double getAxisOffset(Vector3dJME globalAxis);

    public Vector3dJME[] getFaceAxes();
    
    public default Box getEnclosingBox(){
        return getTopLevelBreakDownBox().getEnclosingBox();
    }
}
