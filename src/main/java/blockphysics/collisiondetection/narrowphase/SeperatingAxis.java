
package blockphysics.collisiondetection.narrowphase;

import blockphysics.maths.Vector3dJME;
import java.util.HashMap;
import java.util.Map;


public class SeperatingAxis {
    private final boolean isValidAxis;//where the axis comes from a cross product where both axes were the same the axis becomes (0,0,0) and invalid

    //the axis that is used in the Seperating axis test
    
    private final Vector3dJME globalAxis;
    private final AxisType axisType;
    
    private final Map<NarrowPhaseCollidable,SeperatingAxisLocalFrame> coordinateConversionCacahe=new HashMap<>();
    
    
    @Override
    public String toString() {
        return "SeperatingAxis{" + "globalAxis=" + globalAxis + '}';
    }

    public SeperatingAxis(Vector3dJME globalAxis, AxisType axisType) {
        this.globalAxis = new Vector3dJME(globalAxis);
        this.axisType=axisType;
        
        isValidAxis = globalAxis.lengthSquared()>0;
        
        if(isValidAxis){
            this.globalAxis.normalizeLocal();
        }
    }
    

    public SeperatingAxisLocalFrame getAxisInObjectFrame(NarrowPhaseCollidable object){
        SeperatingAxisLocalFrame frame=coordinateConversionCacahe.get(object);
        
        if(frame==null){
            frame = new SeperatingAxisLocalFrame(object.convertGlobalAxisToLocal(globalAxis),object.getAxisOffset(globalAxis));
            coordinateConversionCacahe.put(object, frame);
        }
        return frame;
    }
    
    public Vector3dJME getAxis(){
        return globalAxis;
    }


    
    public boolean isValidAxis() {
        return isValidAxis;
    }
    
    public boolean isDegenerateWith(SeperatingAxis other){
        return Math.abs(this.globalAxis.x)==Math.abs(other.globalAxis.x)&&
                Math.abs(this.globalAxis.y)==Math.abs(other.globalAxis.y)&&
                Math.abs(this.globalAxis.z)==Math.abs(other.globalAxis.z);
    }

    public AxisType getAxisType() {
        return this.axisType;
    }
    

}
