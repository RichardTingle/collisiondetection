/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.maths.Vector3dJME;



/**
 *
 * @author Richard
 */
public class SeperatingAxisFromCrossProduct extends SeperatingAxis{
    
    private Vector3dJME object1Axis;
    private Vector3dJME object2Axis;
    
    public SeperatingAxisFromCrossProduct(Vector3dJME globalAxis1,Vector3dJME globalAxis2){
        //this is for the seperating axes that are from cross pruducts of axes, this object includes methods to find out what axes those were
        super(globalAxis1.cross(globalAxis2).normalizeLocal(),AxisType.CROSSPRODUCTAXIS);
        
        this.object1Axis=globalAxis1;
        this.object2Axis=globalAxis2;
    }

    public Vector3dJME getOriginatorAxis1() {
        return object1Axis;
    }

    public Vector3dJME getOriginatorAxis2() {
        return object2Axis;
    }


    
}
