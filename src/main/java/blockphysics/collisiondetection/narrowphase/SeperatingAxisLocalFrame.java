/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.maths.Vector3dJME;

/**
 * This is effectively a cache structure that is calculated from a 
 * SeperatingAxis. It is a seperating axis in a particular frame
 * @author Richard
 */
public class SeperatingAxisLocalFrame {
    private final Vector3dJME localAxis;
    private final double offset;  //an axis that has its origin at (0,0,0) in global will have an offset in local coords

    public SeperatingAxisLocalFrame(Vector3dJME localAxis, double zeroPoint) {
        this.localAxis = localAxis.normalize();
        this.offset = zeroPoint;
        

    }

    public Vector3dJME getLocalAxis() {
        return localAxis;
    }

    public double getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "SeperatingAxisLocalFrame{" + "localAxis=" + localAxis + ", offset=" + offset + '}';
    }
    
    
    
    
}
