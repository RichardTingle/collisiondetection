/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

/**
 *
 * @author Richard
 */
public class AxisShadowOverlap extends GlobalAxisShadow{

    private boolean completelyEncloses; //one axis completely encloses the other
    
    public AxisShadowOverlap(double minimum, double maximum, boolean completelyEncloses) {
        super(minimum, maximum);
        this.completelyEncloses=completelyEncloses;
    }

    @Override
    public int hashCode() {
        int hash = 3 + super.hashCode();
        hash = 23 * hash + (this.completelyEncloses ? 1 : 0);
        return hash;
    }

    /**
     * This shadow overlap was created by one axis completely enclosing the other
     * @return 
     */
    public boolean formedFromCompletelyEncloses() {
        return completelyEncloses;
    }

    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        
        
        final AxisShadowOverlap other = (AxisShadowOverlap) obj;
        
        if (this.getStart() != other.getStart() ) {
            return false;
        }
        if (this.getEnd() != other.getEnd() ) {
            return false;
        }
        if (this.completelyEncloses != other.completelyEncloses) {
            return false;
        }
        return true;
    }


    
    
    
}
