/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

import blockphysics.collisiondetection.collisionshape.BreakDownBox;


/**
 *
 * @author Richard
 */
public class PotentialBreakDownBoxCollision{
    private BreakDownBox breakDownBox1;
    private BreakDownBox breakDownBox2;
    public PotentialBreakDownBoxCollision(BreakDownBox box1, BreakDownBox box2){
        if (box1==null || box2==null){
            throw new RuntimeException("Breakdown box cannot be null within a potential collision");
        }

        this.breakDownBox1=box1;
        this.breakDownBox2=box2;

    }



    @Override
    public boolean equals(Object obj){
        if (obj instanceof PotentialBreakDownBoxCollision){
            return breakDownBox1==((PotentialBreakDownBoxCollision)obj).breakDownBox1 && breakDownBox2==((PotentialBreakDownBoxCollision)obj).breakDownBox2;
        }else{
            return false;
        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.breakDownBox1 != null ? this.breakDownBox1.hashCode() : 0);
        hash = 17 * hash + (this.breakDownBox2 != null ? this.breakDownBox2.hashCode() : 0);
        return hash;
    }

    public BreakDownBox getBreakDownBox1() {
        return breakDownBox1;
    }

    public BreakDownBox getBreakDownBox2() {
        return breakDownBox2;
    }
    
    
}

