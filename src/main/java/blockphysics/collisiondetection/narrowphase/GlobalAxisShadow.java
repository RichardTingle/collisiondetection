/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

/**
 *
 * @author Richard
 */
public class GlobalAxisShadow {
    private double start;
    private double end;

    public GlobalAxisShadow( double minimum, double maximum) {
        this.start = minimum;
        this.end = maximum;
    }

    public GlobalAxisShadow() {}
    
    public double getStart() {
        return start;
    }

    public void setStart(double minimum) {
        this.start = minimum;
    }

    public double getEnd() {
        return end;
    }

    public void setEnd(double maximum) {
        this.end = maximum;
    }
    
    @Override
    public String toString(){
        return "Min:"+ start +",Max:" + end;
    }
    
    public double getLength(){
        return Math.abs(end-start);
    }
    
    public double getMinimumOrMaximum(int minOrMax){
        //just for loops that all go from 0 - 1 for min/max
        return (minOrMax==0?start:end);
    }

    /**
     * Returns if this shadow overlaps an other. Assumes that they came from the
     * same axis.
     * 
     * Two shadows that just touch are considered overlapping
     * @param box2Shadow
     * @return 
     */
    public boolean overlapsWith(GlobalAxisShadow other) {
        return this.start <= other.end && other.start <= this.end;
    }
    
    /**
     * Returns a shadow that represents the part on the axis that is shadowed
     * on both axes. This assumes both shadows came from the same axis.
     * 
     * If there is no such overlap (i.e. overlapsWith would return false)
     * then null is returned.
     * 
     * Note! obj1.getOverlapShadow(obj2) is not equivalent to 
     * obj2.getOverlapShadow(obj1) 
     * 
     * This is slightly abstract but is useful for creating the penetration vectors
     * (which are from object1's perspective)
     * 
     * new GlobalAxisShadow(1,10).getOverlapShadow(new GlobalAxisShadow(9,100))
     * is defined as having a positive depth
     * 
     * new GlobalAxisShadow(9,100).getOverlapShadow(new GlobalAxisShadow(1,10))
     * is defined as having a negative depth
     * 
     * @param shadow
     * @return 
     */
    public AxisShadowOverlap getOverlapShadow(GlobalAxisShadow other){
        double overlapStart=Math.max(this.start, other.start);
        double overlapEnd=Math.min(this.end, other.end);
        
        if (overlapEnd<overlapStart){
            return null;
        }else{
            boolean encloses=(this.start==overlapStart && this.end==overlapEnd) || (other.start==overlapStart && other.end==overlapEnd);
            if (this.start<other.start){
                return new AxisShadowOverlap(overlapStart,overlapEnd,encloses);
            }else{
                return new AxisShadowOverlap(overlapEnd,overlapStart,encloses);
            }
            
        }
    }

    double getShadowLength() {
        return Math.abs(getSignedShadowLength());
    }
    double getSignedShadowLength() {
        return end-start;
    }

    public boolean equals(Object obj) {
        return equals(obj,0);
    }
    
    public boolean equals(Object obj, double permittedError) {
        
        if (!(obj instanceof GlobalAxisShadow)){return false;}
        
        GlobalAxisShadow shadow=(GlobalAxisShadow)obj;
        
        if (Math.abs(shadow.start-this.start)>permittedError){return false;}
        if (Math.abs(shadow.end-this.end)>permittedError){return false;}
        
        return true;
        
    }
    
}

