/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

/**
 * Unfortunately the origin of an axis must be known to build the final
 * collision data
 * @author Richard
 */

public enum AxisType {
    COLLIDER1FACEAXIS,COLLIDER2FACEAXIS,CROSSPRODUCTAXIS;

}

