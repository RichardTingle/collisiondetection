/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.narrowphase;

/**
 * These are just objects which would be created and GCed in a tight loop. 
 * Instead these reusable objects are passed around to be reused.
 * 
 * This is a performance optimisation following profiling.
 * 
 * Be careful when using these, there is intentionally no check they have been 
 * returned for performance
 * @author Richs
 */
public class PhysicsReusuableObjects {
    public double[] reusableCorner1=new double[3];
    public double[] reusableCorner2=new double[3];
    public GlobalAxisShadow reusableShadow1=new GlobalAxisShadow();
    public GlobalAxisShadow reusableShadow2=new GlobalAxisShadow();
}
