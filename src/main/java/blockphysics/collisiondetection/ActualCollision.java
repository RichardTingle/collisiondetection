/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection;

import blockphysics.maths.Vector3dJME;



/**
 *
 * @author Richard
 * @param <T>
 */
public class ActualCollision<T> {

    PotentialCollision<T> potentialCollision;
    Vector3dJME collisionPosition;
    Vector3dJME collisionVector;

    public ActualCollision(PotentialCollision<T> potentialCollision,Vector3dJME collisionPosition,  Vector3dJME collisionVector) {

        this.potentialCollision=potentialCollision;
        this.collisionPosition=collisionPosition;
        this.collisionVector = collisionVector;
    }

    public PotentialCollision<T> getPotentialCollision() {
        return potentialCollision;
    }

    public Vector3dJME getCollisionPosition() {
        return collisionPosition;
    }

    public Vector3dJME getCollisionVector() {
        return collisionVector;
    }
    


    
}
