/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.collisionshape;

import blockphysics.collisiondetection.narrowphase.GlobalAxisShadow;
import blockphysics.collisiondetection.narrowphase.PhysicsReusuableObjects;
import blockphysics.collisiondetection.narrowphase.SeperatingAxisLocalFrame;
import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public final class Box {
    private final Vector3dJME minimum;
    private final Vector3dJME maximum;
    
    
    public Box(Vector3dJME minimum, Vector3dJME maximum) {
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public Vector3dJME getMinimum() {
        return minimum;
    }

    public Vector3dJME getMaximum() {
        return maximum;
    }
    
    public double getVolume(){
        return Math.abs(maximum.x-minimum.x)*Math.abs(maximum.y-minimum.y)*Math.abs(maximum.z-minimum.z);
    }
    public Vector3dJME getCentre(){
        return minimum.add(maximum).mult(0.5);
    }
    
    public double getCornerToCornerDistance(){
        double dx=maximum.x-minimum.x;
        double dy=maximum.y-minimum.y;
        double dz=maximum.z-minimum.z;
        
        return Math.sqrt(dx*dx+dy*dy+dz*dz);
    }
    
     /**
     * An axis shadow is the start --> end the object has on that axis. Anywhere 
     * in the plane of that axis.
     * 
     * The below diagram further explains this. Here the * and X represent the 
     * axis and, the X represents the shadow of the box on the axis
     * 
     * 
     *     ---------
     *     |       |  
     *     |       |            *
     *     ---------      X
     *              X
     *        X
     *   *
     * 
     * @param seperatingAxisLocalFrame
     * @param reusuableShadow
     * @param reusableObjects
     * @return 
     */
    public GlobalAxisShadow getBreakdownBoxShadow(SeperatingAxisLocalFrame seperatingAxisLocalFrame,GlobalAxisShadow reusuableShadow, PhysicsReusuableObjects reusableObjects){
        //NOTE!!!! the way this is done, the overlap is for an axis with its 
        //origin at the local (0,0,0), so the overlap needs to be offset
        //higher up the chain
        

        Vector3dJME localAxis=seperatingAxisLocalFrame.getLocalAxis();
        double axisOffset = seperatingAxisLocalFrame.getOffset();
        
        //using the direction of the local axis we can determin which corner will
        //be the min and max of the shadow (where things are paralell it doesn't matter
        //which we choose.
        //This gives 80% saving over going through all corners
        
        int minXIndex=(localAxis.x>0?0:1);
        int minYIndex=(localAxis.y>0?0:1);
        int minZIndex=(localAxis.z>0?0:1);
        
        double minCorner[]=getCorner(minXIndex,minYIndex,minZIndex,reusableObjects.reusableCorner1);
        double maxCorner[]=getCorner(1-minXIndex,1-minYIndex,1-minZIndex,reusableObjects.reusableCorner2);

        reusuableShadow.setStart(localAxis.x*minCorner[0]+localAxis.y*minCorner[1]+localAxis.z*minCorner[2]+axisOffset);
        reusuableShadow.setEnd(localAxis.x*maxCorner[0]+localAxis.y*maxCorner[1]+localAxis.z*maxCorner[2]+axisOffset);           

        return reusuableShadow;
    }
    
        /**
     * Takes 0s and 1s to represent min and max on each axis and returns that 
     * corner of this breakdown box. A bit nasty, but it allows us to completely
     * eliminate branching and this is an internal method
     * @param minMaxX
     * @param minMaxY
     * @param minMaxZ
     * @param reUsableCorner
     * @return 
     */
    protected double[] getCorner(final int minMaxX,final int minMaxY, final int minMaxZ, final double[] reUsableCorner){
            //takes 0s or 1s as minMax for each of X,y, and Z and gives the LOCAL
            //co-ordinates of the corner as a result
            double[] corner=reUsableCorner;

            corner[0]=minMaxX*maximum.x+(1-minMaxX)*minimum.x;
            corner[1]=minMaxY*maximum.y+(1-minMaxY)*minimum.y;
            corner[2]=minMaxZ*maximum.z+(1-minMaxZ)*minimum.z;
            return corner;
    }

    @Override
    public String toString() {
        return "Box{" + "minimum=" + minimum + ", maximum=" + maximum + '}';
    }
    
    
    
}
