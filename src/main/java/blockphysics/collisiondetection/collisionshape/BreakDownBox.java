package blockphysics.collisiondetection.collisionshape;

import blockphysics.collisiondetection.narrowphase.GlobalAxisShadow;
import blockphysics.collisiondetection.narrowphase.PhysicsReusuableObjects;
import blockphysics.collisiondetection.narrowphase.SeperatingAxisLocalFrame;
import blockphysics.maths.Vector3dJME;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BreakDownBox{
    // a breakdown box is a precomputed substructure that contains either
    //annother substructure or the actual box references, used by BoxBasedCollisionShape

    //So, at narrow phase, 2 objects overal boxes collide.
    //objects split in two (what axis and what sizes these two 'halves' are 
    //is precomputed to split the sub boxes as cleanly in two as possible), and 
    //its tested these sub boxes collide and so on, this means each comparison
    //can eliminate half the boxes. Given that objects are only slightly colliding 
    //if colliding, this means each breakdown will typically eliminate half the boxes
    //so rather than a 128 vs 128 box collisions requiring 16384 comparisons we can use 
    //the fact that 128=2^7 to (in an optimal case) find the 1 box vs 1 box that is 
    //actually colliding in 7*7 comparisons=49. Obviously won't be this good in 
    //practice (eg if more that 1 breakdown box is colliding we don't get the 
    //halving. But it'll probably be closer to 49 than to 16384.

    //breakdown boxes substructure should only be calculated when the breakdown box
    //is first required, because at the bottom layers there are a lot of breakdown 
    //boxes, many of which may never be collided with

    private final int minSubBoxSize; //this is the number of boxes within a breakdown box at which it will stop breaking it down
    
    
    //with respect to the local centre unrotated
    private final Box overallBox; //a box that encloses the entire collision shape


    private boolean subStructureCreated=false; //substructure is created on demand
    private Collection<Box> rawBoxes; //this holds the boxes on construction so the substructure can be created on demand
    
    /**
     * In general EITHER there will be the subboxes or the boxesHeldOnThisLevel
     * (when this is the bottom level)
     */
    private BreakDownBox subBox1;
    private BreakDownBox subBox2;
    private BreakDownBox overlapBox;
    private Collection<Box> boxesHeldOnThisLevel;
   
    /**
     * This creates an approximately binary splitting structure of breakdown 
     * boxes. Each layer down approximately halves the number of contained boxes
     * @param boxes 
     * @param minSubBoxSize 
     */
    public BreakDownBox(Collection<Box> boxes, int minSubBoxSize){
        this.rawBoxes=boxes;
        this.minSubBoxSize=minSubBoxSize;
        
        Vector3dJME breakDownOverallMins=new Vector3dJME(Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY);
        Vector3dJME breakDownOverallMaxs=new Vector3dJME(Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY);
       
        for(Box box:boxes){
            breakDownOverallMins.x=Math.min(breakDownOverallMins.x, box.getMinimum().x);
            breakDownOverallMins.y=Math.min(breakDownOverallMins.y, box.getMinimum().y);
            breakDownOverallMins.z=Math.min(breakDownOverallMins.z, box.getMinimum().z);

            breakDownOverallMaxs.x=Math.max(breakDownOverallMaxs.x, box.getMaximum().x);
            breakDownOverallMaxs.y=Math.max(breakDownOverallMaxs.y, box.getMaximum().y);
            breakDownOverallMaxs.z=Math.max(breakDownOverallMaxs.z, box.getMaximum().z);
        }
        
        overallBox = new Box(breakDownOverallMins,breakDownOverallMaxs);
        
    }

    public BreakDownBox(Collection<Box> boxes) {
        this(boxes,4);
    }

    public Collection<Box> getBoxesInThisAndChildren() {
        return rawBoxes;
    }
 

    /**
     * Returns the centre of mass (local coordinates) assuming a uniform density 
     * for each box. Box overlap is not accounted for (boxes shouldn't overlap)
     * @return the centre of mass
     */
    public Vector3dJME getCentreOfMassFromGeometry(){
        //this is the default way that centre of mass is determined, the shape
        //is assumed to be of uniform density
        
        double runningTotalMass=0;
        Vector3dJME runningWeightedPosition=new Vector3dJME (0,0,0);
        
        for(Box box:rawBoxes){
            double boxVolume=box.getVolume();
            Vector3dJME boxCentreOfMassWeighed=box.getCentre().mult(boxVolume);
    
            runningTotalMass+=boxVolume;
            runningWeightedPosition.addLocal(boxCentreOfMassWeighed);
        
        }
        runningWeightedPosition.multLocal(1/runningTotalMass);
        return runningWeightedPosition;
    }
    
    /**
     * Returns a box that encloses entire shape
     * @return 
     */
    public Box getEnclosingBox(){
        return overallBox;
    }
    
    public Vector3dJME getBreakDownOverallMins_localFrame() {
        return overallBox.getMinimum();
    }

    public Vector3dJME getBreakDownOverallMaxs_localFrame() {
        return overallBox.getMaximum();
    }

    

     /**
     * An axis shadow is the start --> end the object has on that axis. Anywhere 
     * in the plane of that axis.
     * 
     * The below diagram further explains this. Here the * and X represent the 
     * axis and, the X represents the shadow of the box on the axis
     * 
     * 
     *     ---------
     *     |       |  
     *     |       |            *
     *     ---------      X
     *              X
     *        X
     *   *
     * 
     * @param seperatingAxisLocalFrame
     * @param reusuableShadow
     * @param reusableObjects
     * @return 
     */
    public GlobalAxisShadow getBreakdownBoxShadow(SeperatingAxisLocalFrame seperatingAxisLocalFrame,GlobalAxisShadow reusuableShadow, PhysicsReusuableObjects reusableObjects){

        return overallBox.getBreakdownBoxShadow(seperatingAxisLocalFrame, reusuableShadow, reusableObjects);
        

    }
    
    /**
     * An axis shadow is the start --> end the object has on that axis. Anywhere 
     * in the plane of that axis.
     * 
     * The below diagram further explains this. Here the * and X represent the 
     * axis and, the X represents the shadow of the box on the axis
     * 
     * 
     *     ---------
     *     |       |  
     *     |       |            *
     *     ---------      X
     *              X
     *        X
     *   *
     * 
     * @param localAxisDirection
     * @param offset
     * @param reusableShadow
     * @return 
     */
    public GlobalAxisShadow getBreakdownBoxShadow(LocalCardinalAxis localAxisDirection,double offset, GlobalAxisShadow reusableShadow){
        reusableShadow.setStart(overallBox.getMinimum().get(localAxisDirection.getNumericEquivalent())+offset);
        reusableShadow.setEnd(overallBox.getMaximum().get(localAxisDirection.getNumericEquivalent())+offset);
        return reusableShadow;
    }


    public boolean isBottomLevelBox(){
        ensureSubStructureCreated();
        if (subBox1==null){
            return true;
        }else{
            if (subBox2==null){
                throw new RuntimeException("BreakdownBox: inconsistent substructure");
            }
            return false;
        }
    }
    
    /**
     * Returns if the breakdown box has a third substructure box that contains 
     * boxes that span both breakdown regions 
     * @return 
     */
    public boolean hasOverlapBox(){
        ensureSubStructureCreated();
         return overlapBox != null && overlapBox.getNumberOfContainedBoxes()!=0;
    }

    public BreakDownBox getSubBox1(){
        ensureSubStructureCreated();
        return subBox1; //if botttom level, will be null
    }
    public BreakDownBox getSubBox2(){
        ensureSubStructureCreated();
        return subBox2; //if botttom level, will be null
    }
    public BreakDownBox getOverlapBox(){
        ensureSubStructureCreated();
        return overlapBox; //if botttom level, will be null
    }
    public Collection<Box> getNonBoxedReferences(){
        ensureSubStructureCreated();
        return boxesHeldOnThisLevel; //if botttom level will contain everything
    }

    private void ensureSubStructureCreated(){
        //the substructure is only created upon demand, so if we ask for the 
        //substructure we need to check its been created
        
        if (subStructureCreated==false){
            createSubstructure(this.rawBoxes);
        }
        subStructureCreated=true;
    }


    private void createSubstructure(Collection<Box> boxes){
        //creates the 2 (or 3) sub boxes below this one in the structure
        if (boxes.size()<=minSubBoxSize){
            //stop breaking down, hold all boxes on this level
            boxesHeldOnThisLevel=boxes;
        }else{
            //find the x,y or z that breaks the box the closest to in half
            LocalCardinalAxis bestAxis=null;
            SplitPoint bestSplitPoint=null;

            for(LocalCardinalAxis axis:LocalCardinalAxis.values()){
                SplitPoint tempSplitPoint=getSplitPoint(axis,boxes);
                if (bestSplitPoint==null ||  tempSplitPoint.boxingFraction>bestSplitPoint.boxingFraction){
                    bestSplitPoint=tempSplitPoint;
                    bestAxis=axis;
                }
            }

            SplitSet splitSet=getSplitSet(bestAxis, boxes, bestSplitPoint.splitPoint);

            //the splitSet contains all the information as to which sub breakdown box the boxes should go
            if (splitSet.isOnewaySet()){
                //defence against parycitic case where only 1 box is filled
                this.boxesHeldOnThisLevel=boxes;
            }else{
                subBox1=new BreakDownBox(splitSet.getLeftBoxes());
                subBox2=new BreakDownBox(splitSet.getRightBoxes());
                
                
                Collection<Box> overlapBoxes=splitSet.getOverlapBoxes();
                if (!overlapBoxes.isEmpty()){
                    overlapBox=new BreakDownBox(overlapBoxes);
                }
                
            }

        }

    }



    /**
     * On the requested axis gets the split point on the axis that most closely splits the 
     * boxes in two. This is an expensive operation O(n^2) but it is only done
     * once for the entire lifetime of the collision shape so it is a sensible 
     * optimisation
     * @param axis
     * @param boxReferences
     * @return 
     */
    private SplitPoint getSplitPoint(LocalCardinalAxis axis,Collection<Box> boxs){
        Set<Double> splitPoints=new HashSet<>();

        //each start/end point of a box is a potential split point
        for(Box box:boxs){
            splitPoints.add(box.getMinimum().get(axis.getNumericEquivalent()));
            splitPoints.add(box.getMaximum().get(axis.getNumericEquivalent()));
        }

        //now run through each split point and find the one that gives the best boxing fraction
        //(i.e. the one that most closesly splits them in two

        Iterator<Double> splitPointsIterator=splitPoints.iterator();

        double bestBoxingFaction=0;
        double bestsplitPoint=0;

        while(splitPointsIterator.hasNext()){
            double tempSplitPoint=splitPointsIterator.next();
            double tempBoxingFraction=getBoxingFraction(axis,boxs,tempSplitPoint);

            if (tempBoxingFraction>bestBoxingFaction){
                bestsplitPoint=tempSplitPoint;
                bestBoxingFaction=tempBoxingFraction;
            }
        }
        return new SplitPoint(bestsplitPoint,bestBoxingFaction);
    }

    /**
     * Given the provided boxes, axis and proposed splitPoint returns:
     * The worse (i.e. lower) of:
     *      The fraction of boxes in the left box (minus any fraction in the overlap box)
     *      The fraction of boxes in the right box (minus any fraction in the overlap box)
     * 
     * This means the larger this number the better the splitting (the maximum 
     * that will be returned in 0.5 which represents perfect splitting)
     * 
     * The minus any fraction in the overlap box strongly penalises 
     * (effectively double penalises) overlaps as they are caustic to the 
     * breakdown box algorithm as they are not split at all by the break down
     * 
     * @param axis
     * @param boxReferences
     * @param splitPoint
     * @return 
     */
    private double getBoxingFraction(LocalCardinalAxis axis,Collection<Box> boxes, double splitPoint ){
        //this puts the boxes into 3 catorgaries: spanning the splitpoint, 
        //left of splitPoint or right of the split point. 

        int leftBox=0;
        int rightBox=0;
        int overlap=0;

        for(Box box:boxes){
            if (box.getMaximum().get(axis.getNumericEquivalent())<=splitPoint){
                leftBox++;
            }else if (box.getMinimum().get(axis.getNumericEquivalent())>=splitPoint){
                rightBox++;
            }else{
                overlap++;
            }
        }
        
        double leftBoxingFraction=((double)leftBox)/boxes.size();
        double rightBoxingFraction=((double)rightBox)/boxes.size();
        double overlaBoxingFraction=((double)overlap)/boxes.size();

        //overlap is caustic to the algorithm, significant overlap must be 
        //penalised, a slighly less even boxing with less overlap would be desirable
        if (leftBoxingFraction<rightBoxingFraction){
            return leftBoxingFraction-overlaBoxingFraction;
        }else{
            return rightBoxingFraction-overlaBoxingFraction;
        }

    }

    private SplitSet getSplitSet(LocalCardinalAxis axis,Collection<Box> boxes, double splitPoint){
        SplitSet splitSet=new SplitSet();

        for(Box box:boxes){
            if (box.getMaximum().get(axis.getNumericEquivalent())<=splitPoint){
                splitSet.addToLeftBox(box);
            }else if (box.getMinimum().get(axis.getNumericEquivalent())>=splitPoint){
                splitSet.addToRightBox(box);
            }else{
                //it overlaps
                splitSet.addToOverlap(box);
            }
            
        }
        return splitSet;
    }

    /**
     * Takes 0s and 1s to represent min and max on each axis and returns that 
     * corner of this breakdown box. A bit nasty, but it allows us to completely
     * eliminate branching and this is an internal method
     * @param minMaxX
     * @param minMaxY
     * @param minMaxZ
     * @param reUsableCorner
     * @return 
     */
    private double[] getCorner(final int minMaxX,final int minMaxY, final int minMaxZ, final double[] reUsableCorner){
            return overallBox.getCorner(minMaxX, minMaxY, minMaxZ, reUsableCorner);
    }

    private int getNumberOfContainedBoxes() {
        return this.rawBoxes.size();
    }

    
    
    private class SplitPoint{
        //glorified struct
        public double splitPoint;
        public double boxingFraction;
        public SplitPoint(double splitPoint, double boxingFraction){
            this.splitPoint=splitPoint;
            this.boxingFraction=boxingFraction;
        }
        
        

    }
    private class SplitSet{
        
        private final Collection<Box>leftBox=new HashSet<>(); //stuff to the 'left' of the split point on the relevant axis
        private final Collection<Box>rightBox=new HashSet<>(); //to the right
        private final Collection<Box>overlap=new HashSet<>();  //stuff that overlaps (or if bottom layer: everything)
       
        public SplitSet(){}
        
        public void addToLeftBox(Box boxRef){
            leftBox.add(boxRef);
        }
        public void addToRightBox(Box boxRef){
            rightBox.add(boxRef);
        }
        public void addToOverlap(Box boxRef){
            overlap.add(boxRef);
        }
        public Collection<Box> getLeftBoxes(){
            return leftBox;
        }
        public Collection<Box> getRightBoxes(){
            return rightBox;
        }
        public Collection<Box> getOverlapBoxes(){
            return overlap;
        }
        public boolean isOnewaySet(){
            //if the set has only occupation in box 1 or box 2, then this
            //is the bottom set, i'm not sure if this scenario could ever 
            //occur, but putting in this test ensures that a rare case doesn't
            //cause exception

            if (leftBox.isEmpty()){
                return true;
            }
            if (rightBox.isEmpty()){
                return true;
            }
            return false;
        }

        


    }
}

