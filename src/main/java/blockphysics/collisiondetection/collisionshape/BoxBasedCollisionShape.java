
package blockphysics.collisiondetection.collisionshape;

import blockphysics.maths.Vector3dJME;
import java.util.ArrayList;
import java.util.Collection;


/**
 *
 * @author Richard
 */
public class BoxBasedCollisionShape {
       

    //this is for broadphase, this+maxRadius defines a region in which collisions can occure
    private final Vector3dJME localGeometricalCentre;
    private final double maxRadius;
    
    //this is for midPhase
    private final Collection<Vector3dJME> cornerPoints;
    
    //this is for narrowPhase
    private final BreakDownBox breakDownBox;
    
    public  BoxBasedCollisionShape(Collection<Box> boxes){

        
        breakDownBox=new BreakDownBox(boxes);
      
        Vector3dJME overallMins=breakDownBox.getBreakDownOverallMins_localFrame();
        Vector3dJME overallMaxs=breakDownBox.getBreakDownOverallMaxs_localFrame();
              

        localGeometricalCentre=overallMins.add(overallMaxs).mult(0.5);

        maxRadius=overallMaxs.subtract(overallMins).mult(0.5).length();
        
        cornerPoints=new ArrayList<>(8);
        for(int i=0;i<2;i++){
            double x=(i==0?overallMins.x:overallMaxs.x);
            for(int j=0;j<2;j++){
                double y=(j==0?overallMins.y:overallMaxs.y);
                for(int k=0;k<2;k++){
                    double z=(k==0?overallMins.z:overallMaxs.z);
                    cornerPoints.add(new Vector3dJME(x,y,z));
                }
            }
        }
        
    }

    @Override
    public String toString() {
        return "BoxBasedCollisionShape{" + "overallMins=" + breakDownBox.getBreakDownOverallMins_localFrame() + ", overallMaxs=" + breakDownBox.getBreakDownOverallMaxs_localFrame() + ", localGeometricalCentre=" + localGeometricalCentre + '}';
    }
   
    public BreakDownBox getTopLevelBreakDownBox(){
        return breakDownBox;
    }
    
    /**
     * 
     * @return 
     */
    public Vector3dJME getLocalGeometricalCentre_localFrame(){
        //this is the geometrical centre that minimises maxRadius
        return localGeometricalCentre;
    }
    
    /**
     * Returns the centre of mass (in local coordinates) under the assumption
     * that the collision shape has a uniform density
     * @return 
     */
    public Vector3dJME getCentreOfMassFromGeometry_localFrame(){
        return breakDownBox.getCentreOfMassFromGeometry();
    }
    /**
     * Together the geometric centre and the maxRadiusFromGeometricalCentre
     * a sphere that completely encloses the shape is defined (it should 
     * additionally be as small as possible)
     * @return 
     */
    public double getmaxRadiusFromGeometricalCentre(){
        return maxRadius;
    }
    
    public Collection<Vector3dJME> getCornerPoints(){
        //this returns the 8 corners of the overall containing box
        //NOTE!!!! relative to local centre (which is the point that minimises
        //the maximum radius, used for collision detection, not centre of mass
        return cornerPoints;
        
    }



   

}
