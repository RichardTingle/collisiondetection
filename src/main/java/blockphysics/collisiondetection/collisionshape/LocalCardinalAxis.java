/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.collisionshape;

/**
 *
 * @author Richs
 */
public enum LocalCardinalAxis {
    X(0),Y(1),Z(2);
    
    private final int numericEquivalent;
    
    LocalCardinalAxis(int numericEquivalent){
        this.numericEquivalent=numericEquivalent;
    }

    /**
     * Some performance or code readability enhancements use a numeric equivalent
     * of the axes. This is consident throughout the system (e.g. Vector3dJME uses 
     * the same index equivalents)
     * @return reability
     */
    public int getNumericEquivalent() {
        return numericEquivalent;
    }
    
}
