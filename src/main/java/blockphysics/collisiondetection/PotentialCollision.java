/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection;



/**
 *
 * @author Richard
 */
public class PotentialCollision<T> {
    private final T collider1;
    private final T collider2;

    public PotentialCollision(T collider1, T collider2) {
        //colliders are always ordered with the lowest hash first, what the hash
        //is doesn't really matter, it just ensures that they are in a consistent 
        //order so that further comparisons work (neccissary for axis corrilation)
        
        if(collider1.hashCode()<collider2.hashCode()){
            this.collider1 = collider1;
            this.collider2 = collider2;
        }else{
            this.collider1 = collider2;
            this.collider2 = collider1;
        }
        
        
    }

    public T getCollider1() {
        return collider1;
    }

    public T getCollider2() {
        return collider2;
    }
    

    
    
    @Override
    public boolean equals(Object obj){
        if (obj instanceof PotentialCollision){
            PotentialCollision potentialCollisions=(PotentialCollision)obj;
            //looking for reference equality not just equivalence
            return potentialCollisions.collider1==this.collider1 && potentialCollisions.collider2==this.collider2;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.collider1 != null ? this.collider1.hashCode() : 0);
        hash = 83 * hash + (this.collider2 != null ? this.collider2.hashCode() : 0);
        return hash;
    }

}
