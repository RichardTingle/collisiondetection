/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;

import blockphysics.collisiondetection.PotentialCollision;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 *
 * @author Richs
 * @param <BroadPhaseCollidableTYPE>
 */
public class BroadPhaseAxisSweep<BroadPhaseCollidableTYPE extends BroadPhaseCollidable> implements Callable<Set<PotentialCollision<BroadPhaseCollidableTYPE>>>  {
    private final List<BroadPhaseCollidableTYPE> axisIndicies;

    //orders the same as axisIndicies, caches
    private double[] starts;
    private boolean[] isStatic;
    
    /**
     * These are algorithms which are selected depending on the axis
     */
    private final AxisPointDeterminingAlgorithm startPointOnAxis;
    private final AxisPointDeterminingAlgorithm endPointOnAxis;
    
    private final SweepAxis axis;

    
    /**
     * 
     * @param axis The axis to do the sweep over
     * @param collidables_preAxisSorted the collidables must be presorted along the axis specified
     */
    public BroadPhaseAxisSweep(SweepAxis axis, List<BroadPhaseCollidableTYPE> collidables_preAxisSorted) {
        this.axis=axis;
        this.axisIndicies =collidables_preAxisSorted;

        switch(axis){
            case XAXIS:
                startPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().x-broadPhaseCollidable.getMaxRadius(); 
                endPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().x+broadPhaseCollidable.getMaxRadius(); 
                break;
            case YAXIS:
                startPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().y-broadPhaseCollidable.getMaxRadius(); 
                endPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().y+broadPhaseCollidable.getMaxRadius();
                break;    
            case ZAXIS:
                startPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().z-broadPhaseCollidable.getMaxRadius(); 
                endPointOnAxis = broadPhaseCollidable -> broadPhaseCollidable.getCollisionalCentrePosition().z+broadPhaseCollidable.getMaxRadius();
                break;    
            default:
                throw new AssertionError(axis.toString() + " is not a supported axis");
                        
                
        }
    }

    /**
     * Prepares the data caches for performance
     */
    private void prepareData(){ 
        
        starts=new double[axisIndicies.size()];
        isStatic=new boolean[axisIndicies.size()];
        for(int i=0;i<axisIndicies.size();i++){
            starts[i]=startPointOnAxis.getPoint(axisIndicies.get(i));
            isStatic[i]=axisIndicies.get(i).isStatic();
        }
    }
    
    @Override
    public Set<PotentialCollision<BroadPhaseCollidableTYPE>> call() throws Exception {
        prepareData();
        return axisSweep();
    }
    
    private Set<PotentialCollision<BroadPhaseCollidableTYPE>> axisSweep(){
        
        Set<PotentialCollision<BroadPhaseCollidableTYPE>> thisSweep =new HashSet<>();

        //startXsIndicies.length== startYsIndicies.length==startZsIndicies.length;
        for(int i=0;i<starts.length;i++){
            BroadPhaseCollidable activeObject=axisIndicies.get(i);
            if (activeObject.isCollidable()){
                double activeObjectEnd=endPointOnAxis.getPoint(activeObject);
                //sweep forwards until an objects start is before out end
                for(int j=i+1;j<starts.length;j++){
                    BroadPhaseCollidable testObject=axisIndicies.get(j);
                    //if both objects are static or both are light weight then they cannot by definition collide, we can skip
                        
                    if (!((isStatic[i] && isStatic[j]) || (!activeObject.isCollidable() || !testObject.isCollidable()))){
                        if (activeObjectEnd>starts[j]){
                            //there is overlap on this axis, add it to the possibles
                            PotentialCollision potentialCollision=new PotentialCollision(activeObject,testObject);
                            thisSweep.add(potentialCollision);
                        }else{
                            break; //this is as far as this active object goes, we're no longer in the overlap and can move to the next objecr

                        }
                    }
                }
            }
        }
        
        return thisSweep;
    }


    

    @FunctionalInterface 
    private interface AxisPointDeterminingAlgorithm<BroadPhaseCollidableTYPE extends BroadPhaseCollidable>{
        double getPoint(BroadPhaseCollidableTYPE object);
    }
     


    

    
}
