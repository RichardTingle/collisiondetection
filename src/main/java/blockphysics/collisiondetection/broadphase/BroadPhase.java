/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;



import blockphysics.collisiondetection.PotentialCollision;
import blockphysics.maths.sorting.InsertionSort;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Richard
 * @param <BroadPhaseCollidableTYPE>
 */
public class BroadPhase<BroadPhaseCollidableTYPE extends BroadPhaseCollidable> {
    /*
    * The broadphase is based on the sweep and prune algorithm, 
    * see http://en.wikipedia.org/wiki/Sweep_and_prune
    *
    * By making the BroadPhase generic we can have it only require 
    * BroadPhaseCollidable while potentially being open to returning potential 
    * collisions made up of more complex types. Giving us the best of both worlds
   */
   
    
    private final Object newObjectLock=new Object(); //we want to lock while the newObjects are being added to the main objects to avoid any possibility of annother thread sneaking an object in
    
    /*
    * Physics objects can be requested to be added/removed at any time but this 
    * is only actioned at the beginning of each phase
    */
    private final Set<BroadPhaseCollidableTYPE> newObjects=new HashSet<>(); //things that were added in the last cycle
    
    /*
    * these hold the objects in a list sorted such that they are ordered in
    * accending order on the respective axes
    */
    private final List<BroadPhaseCollidableTYPE> startXsIndicies=new ArrayList<>();
    private final List<BroadPhaseCollidableTYPE> startYsIndicies=new ArrayList<>();
    private final List<BroadPhaseCollidableTYPE> startZsIndicies=new ArrayList<>();
    
    private final ExecutorService threadPool;
    
    private final InsertionSort<BroadPhaseCollidableTYPE> sorter=new InsertionSort<>();
    
    /**
     * The broadphase runs multithreaded. For optimum performance this 
     * dependancy should be shared with all other parts of the program and 
     * should have a number of threads equal to the number of logical cores of
     * the CPU
     * @param sweepPool 
     */
    public BroadPhase(ExecutorService sweepPool){
        this.threadPool=sweepPool;
    }
    
    public void registerObject(BroadPhaseCollidableTYPE object){
        synchronized (newObjectLock) {
            newObjects.add(object);
        }
        
    }


    
    public Collection<PotentialCollision<BroadPhaseCollidableTYPE>> runBroadPhase(){
        removeAndAddItems();
        sortIndicies();
        
        return generatePotentialCollisions();
    }
    
    private void removeAndAddItems(){
        synchronized (newObjectLock) {
            startXsIndicies.addAll(newObjects);
            startYsIndicies.addAll(newObjects);
            startZsIndicies.addAll(newObjects);
            newObjects.clear();
        }
        
        for(BroadPhaseCollidableTYPE collidable:startXsIndicies){
            if(!collidable.isAlive()){
                startXsIndicies.remove(collidable);
                startYsIndicies.remove(collidable);
                startZsIndicies.remove(collidable);
            }
        }

    }
    
    
    private void sortIndicies(){

        /*
        *sort so that the objects are ordered such that their "left" most points
        *are in order
        */
        //If this proves a bottle neck could be run in parallel
        sorter.sort(startXsIndicies, (broadPhaseCollidable) -> (long)(broadPhaseCollidable.getCollisionalCentrePosition().x - broadPhaseCollidable.getMaxRadius()));
        sorter.sort(startYsIndicies, (broadPhaseCollidable) -> (long)(broadPhaseCollidable.getCollisionalCentrePosition().y - broadPhaseCollidable.getMaxRadius()));
        sorter.sort(startZsIndicies, (broadPhaseCollidable) -> (long)(broadPhaseCollidable.getCollisionalCentrePosition().z - broadPhaseCollidable.getMaxRadius()));
        
    } 

    private Collection<PotentialCollision<BroadPhaseCollidableTYPE>> generatePotentialCollisions(){

        try {
            
            //expensive for large numbers of objects, multithread
            Future<Set<PotentialCollision<BroadPhaseCollidableTYPE>>> futureX=threadPool.submit(new BroadPhaseAxisSweep(SweepAxis.XAXIS,startXsIndicies));
            Future<Set<PotentialCollision<BroadPhaseCollidableTYPE>>> futureY=threadPool.submit(new BroadPhaseAxisSweep(SweepAxis.YAXIS,startYsIndicies));
            Future<Set<PotentialCollision<BroadPhaseCollidableTYPE>>> futureZ=threadPool.submit(new BroadPhaseAxisSweep(SweepAxis.ZAXIS,startZsIndicies));
             
            
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> xSweep=futureX.get();
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> ySweep=futureY.get();
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> zSweep=futureZ.get();
            /*
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> xSweep=new BroadPhaseAxisSweep(SweepAxis.XAXIS,startXsIndicies).call();
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> ySweep=new BroadPhaseAxisSweep(SweepAxis.YAXIS,startYsIndicies).call();
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> zSweep=new BroadPhaseAxisSweep(SweepAxis.ZAXIS,startZsIndicies).call();
            */
            
            return correlateSweeps(xSweep, ySweep,zSweep);
            
            
            
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(BroadPhase.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("BroadPhase failed while in multithread",  ex);
        } catch (Exception ex) {
            Logger.getLogger(BroadPhase.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("BroadPhase failed while in multithread",  ex);
        }
    }

    /**
     * Returns potential collisions that occur on all axes (so could occur)
     * @param xSweep
     * @param ySweep
     * @param zSweep
     * @return 
     */
    private Collection<PotentialCollision<BroadPhaseCollidableTYPE>> correlateSweeps(Set<PotentialCollision<BroadPhaseCollidableTYPE>> xSweep, Set<PotentialCollision<BroadPhaseCollidableTYPE>> ySweep,Set<PotentialCollision<BroadPhaseCollidableTYPE>> zSweep){
        Set<PotentialCollision<BroadPhaseCollidableTYPE>> small=xSweep;
        Set<PotentialCollision<BroadPhaseCollidableTYPE>> medium=ySweep;
        Set<PotentialCollision<BroadPhaseCollidableTYPE>> large=zSweep;

        //rolled out sorting algorithm
        if (small.size()>medium.size()){
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> temp=medium;
            medium=small;
            small=temp;
        }
        if (medium.size()>large.size()){
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> temp=large;
            large=medium;
            medium=temp;
        }
        if (small.size()>medium.size()){
            Set<PotentialCollision<BroadPhaseCollidableTYPE>> temp=medium;
            medium=small;
            small=temp;
        } 
        
        //profiling showed this to be quicker than doing 2 retainAlls
        Collection<PotentialCollision<BroadPhaseCollidableTYPE>> returnCol=new ArrayList<>();
        for (PotentialCollision pc : small) {
            if(medium.contains(pc) && large.contains(pc)){
                returnCol.add(pc);
            }
        }
        
        return returnCol;
    }
    

    
    




}
