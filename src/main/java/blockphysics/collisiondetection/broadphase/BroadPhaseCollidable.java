/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockphysics.collisiondetection.broadphase;

import blockphysics.maths.Vector3dJME;

/**
 *
 * @author Richs
 */
public interface BroadPhaseCollidable {
    /**
     * Once an object declares itself not alive it is removed at the next phase
     * @return 
     */
    boolean isAlive();
    
    /**
     * A point considered the centre of the collidable.
     * DOES NOT NEED TO BE (but may be) the Centre of Mass
     * @return 
     */
    Vector3dJME getCollisionalCentrePosition();
    
    /**
     * The entire object must be within maxRadius of the collisionalCentrePosition
     * @return 
     */
    double getMaxRadius();

    public boolean isStatic();

    public boolean isCollidable();
}
