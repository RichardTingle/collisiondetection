## What is this repository for? ##

This repository contains the collision detection pathway, part of the BlockPhysics physics engine. Its input is the current state of all the physics objects, its output is a set of collisions which are sent to the collision resolver to pull out of collision (if possible, otherwise minimizing the collision depth).

A naive approach to collision detection implies an O(n^2) process (where n is the number of physics objects) whereby each object is checked against each other object. This is simple but impractical for large scenes. This collision detection system instead has 3 phases where collisions are assessed as increasingly more likely:

![blockCollisions.png](https://bitbucket.org/repo/4Lk7de/images/4120985320-blockCollisions.png)

### Broadphase ###

This is implemented as an axis sweep. Each axis (x,y,z) maintains an ordered list of the objects first possible (i.e. at any rotation) starting point. This list is maintained between sweeps so that it always remains "nearly sorted", only small object movements making it not entirely sorted. As a nearly sorted list the InsertionSort is used to fully sort it (In general the InsertionSort has very bad performance but with a nearly sorted list its performance is good). 

![broadphaseWithBorders.png](https://bitbucket.org/repo/4Lk7de/images/2920219158-broadphaseWithBorders.png)

Once a fully sorted list is obtained it is possible to sweep down each axis and determine if **on that axis** the physics objects could overlap (assuming some rotation). This is approximately O(n) as each object only needs to be assessed against the next few objects in the list (until their "starts" start coming after the assessment object's "end"). As these axis sweeps are entirely independent this is an excellent part of the code to be run in parallel.

After the 3 sweeps are run they are correlated. Any potential collision that has an overlap on every axis is considered to have passed the broadphase and move on to the midphase.  

### Midphase ###

The mid phase considers each potential collision coming from the broadphase independently and makes a more aggressive assessment of whether a collision has occurred. The midphase also assesses the maximum extents along each axis for each object. However unlike the broadphase it considers this given the objects current rotation.

![midphase.png](https://bitbucket.org/repo/4Lk7de/images/2022351292-midphase.png)

### Narrowphase ###

The Narrowphase accepts all potential collisions from the Midphase and attempts to answer if they in fact are actual collisions. Physics objects in BlockPhysics are considered to be made up of arbitrary sized but potentially many boxes to support a block world (These boxes are preoptimised by joining potentially 10s of thousands of blocks into usually around a 100 larger boxes).

The Narrowphase uses 2 key techniques

* The separating axis theorem which allows collisions to be determined. It is based upon the fundamental principle that "If two convex objects are not colliding then there must be some axis upon which they have no overlap". The physics objects in BlockPhysics engine are not required to be convex but the individual boxes that make them up are so this requirement is fulfilled. As per the separating axis theorem in 3D space the only axes that need be considered are; the 3 face normals of the 3 colliding objects (so 6 in total) plus the cross product of each of those (a further 9 axes).
* The breakdown phase, a naive approach to applying the separating axis theorem would have each box assessed against each box. This would lead to O(m*o) assessments (where m is the number of boxes in the first collider and o is the number of boxes in the second collider. With typical numbers of boxes in the hundreds this is impractical. The breakdown approach attempts to create a nested structure that groups boxes into approximately 2 at each level; creating a deep dtructure that allows (in ideal circumstances) for the number of potential box level collisions to be halved on each condition. This breakdown structure is expensive to create however it is cached between sweeps reducing that cost and only created on demand; reducing start up time, much of the breakdown structure may never be created.

![narrowphase.png](https://bitbucket.org/repo/4Lk7de/images/1345338189-narrowphase.png)

## Overall design ##
Each phase is written using generics to use (for example) <T extends MidPhaseCollidable>. This has a number of benefits:

* This makes testing easy; just a MidPhaseCollidable mock has to be created. 
* The MidPhase can be easily used elsewhere, without dragging the requirements of the the other collision phases into that new location.
* The use of generics allow a more complex T to be typesafely carried through the entire process; i.e. the output of the broadphase can (without typecasting) be given to the midphase.